/*
 * Copyright (c) 2003, Vanderbilt University
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice, the following
 * two paragraphs and the author appear in all copies of this software.
 * 
 * IN NO EVENT SHALL THE VANDERBILT UNIVERSITY BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 * OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE VANDERBILT
 * UNIVERSITY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE VANDERBILT UNIVERSITY SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE VANDERBILT UNIVERSITY HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 * Author: Shourya Singh Gupta
 * Date last modified: 25/12/14
 * Author: Gyorgy Balogh, Gabor Pap, Miklos Maroti
 * Date last modified: 02/09/04
 */
 
package net.tinyos.prowler;

import java.awt.Color;
import java.awt.Graphics;
import static java.lang.Math.abs;
import static java.lang.Math.floor;
import java.lang.Math;
import java.util.*;
import matlabcontrol.LoggingMatlabProxy;
import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.extensions.MatlabProxyLogger;

/**
 * This is a sample application, it shows a way of utilizing the Prowler 
 * simulator. This is a broadcast application, where a mote in the middle of the 
 * field broadcasts a message which is further broadcasted by all the recepients.
 * Please note that this is not the only way of writing applications, this is 
 * just an example.
 * 
 * @author Gabor Pap, Gyorgy Balogh, Miklos Maroti
 */
public class TestBroadcastNode extends Mica2Node {
    
        
        public static MatlabProxyFactory factory;
        
        public static MatlabProxy proxy;
    
        //public int[] idsReceived = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        
        //public ArrayList<Integer> messagesRcvd = new ArrayList();
        
        /** This field is true if this mote rebroadcasted the message already. */
	boolean sent = false;

	/** This field stores the mote from which the message was first received. */
	private Node parent = null; 

	/**
	 * This extension of the {@link Application} baseclass does everything we 
	 * expect from the broadcast application, simply forwards the message once,
	 * and that is it. 
	 */
	public class BroadcastApplication extends Application{
		
		/**
		 * @param node the Node on which this application runs.
		 */
		public BroadcastApplication(Node node) {
			super(node);
		}
    
		/**
		 * Stores the sender from which it first receives the message, and passes 
		 * the message.
		 */
		public void receiveMessage(Object messages, ArrayList coeffs) throws Exception, MatlabConnectionException, MatlabInvocationException{
                    if(!done)
                    {
                    Integer nodeID = (Integer)id;
                    int messageLen = ((ArrayList)messages).size();
                    //System.out.println("Message Len = "+messageLen);
                    int idx = 0;
                    Double next_rank = null;
                    while(messageLen>0)
                    {
                        Integer len = ((ArrayList)(coeffs.get(idx))).size();
                        Integer i = 1;
                        String compute = "";
                        while(len>0)
                        {
                            compute = compute.concat("A("+((Integer)(((ArrayList)(coeffs.get(idx))).get(i-1))).toString()+")*temp_Coeffs"+(((ArrayList)messages).get(idx)).toString()+"("+i.toString()+",:)");
                            len--;
                            i++;
                            if(len!=0)
                            compute = compute.concat("+");
                        }
                        //System.out.println(compute);
                        proxy.eval(compute);
                        proxy.eval("temp_compute = ans");
                        double temp = ((double[]) proxy.returningEval("rank(temp_Coeffs"+((Integer)id).toString()+")", 1)[0])[0];
                        Double prev_rank = (Double)temp;
                        proxy.eval("temp_Coeffs"+nodeID.toString()+" = [temp_Coeffs"+nodeID.toString()+";temp_compute]");
                        temp = ((double[]) proxy.returningEval("rank(temp_Coeffs"+((Integer)id).toString()+")", 1)[0])[0];
                        next_rank = (Double)temp;
                        proxy.eval("arr_size = size(temp_Coeffs"+((Integer)id).toString()+")");
                        proxy.eval("temp_Coeffs"+((Integer)id).toString()+" = temp_Coeffs"+((Integer)id).toString()+"(1:(arr_size(1)-1),:)");
                        if(next_rank>prev_rank)
                        {
                            proxy.eval("temp_Coeffs"+nodeID.toString()+" = [temp_Coeffs"+nodeID.toString()+";temp_compute]");
                            proxy.eval("temp_compute.*[message1 message2 message3 message4 message5 message6 message7 message8 message9 message10 message11 message12 message13 message14 message15 message16]");
                            proxy.eval("ans(1) + ans(2) + ans(3) + ans(4) + ans(5) + ans(6) + ans(7) + ans(8) + ans(9) + ans(10) + ans(11) + ans(12) + ans(13) + ans(14) + ans(15) + ans(16)");
                            proxy.eval("temp_messages"+nodeID.toString()+" = [temp_messages"+nodeID.toString()+";ans]");
                        }
                        //proxy.eval("ans.*[message1 message2 message3 message4 message5 message6 message7 message8 message9 message10 message11 message12 message13 message14 message15 message16]");
                        //proxy.eval("ans(1) + ans(2) + ans(3) + ans(4) + ans(5) + ans(6) + ans(7) + ans(8) + ans(9) + ans(10) + ans(11) + ans(12) + ans(13) + ans(14) + ans(15) + ans(16)");
                        //proxy.eval("messages"+nodeID.toString()+" = [messages"+nodeID.toString()+";ans]");
                        messageLen--;
                        idx++;
                    }
                    
                    //double temp2 = ((double[]) proxy.returningEval("rank(Coeffs"+((Integer)id).toString()+")", 1)[0])[0];
                    //Double rank = (Double)temp2;
                    System.out.println("Rank of node "+id+" is "+next_rank.intValue());
                    //matrixRank.add(id, rank.intValue());
                    matrixRank.set(id-1,next_rank.intValue());
                    
                    double temp1 = ((double[]) proxy.returningEval("size(temp_Coeffs"+((Integer)id).toString()+",1)", 1)[0])[0];
                    //double matrixLen =  ((double[])logger.returningEval("size(temp,1)", 1))[0];
                    Double matrixLen = (Double)temp1;
                    //System.out.println("Matrix Length is :"+matrixLen);
                    ArrayList randoms= new ArrayList();
                    Random randomGenerator = new Random();
                    for (int i = 0; i < matrixLen; ++i)
                    {
                        Integer randomInt = randomGenerator.nextInt(256)+1;
                        randoms.add(randomInt);
                        //System.out.println("Generated : " + randomInt);
                    }
                    boolean check = false;
                    for(int i=0; i<matrixRank.size();i++)
                    {
                        if((int)matrixRank.get(i)!=16)
                        {
                           System.out.println("Node "+(i+1)+" has not yet reached rank 16. Its rank is "+matrixRank.get(i));
                           check = true;
                           break;
                        }
                    }
                    if(check)
                    {
                        //System.out.println("Is it sending");
                        sendMessage(id,randoms);   
                    }
                    else
                    {
                        System.out.println("Done!!");
                        done = true;
                    }
                        
                    }
                }    

		/**
		 * Sets the sent flag to true. 
		 */
		public void sendMessageDone(){
			sent = true;       
                        messages.clear();
                        coeffs.clear();
		}
	}

	public TestBroadcastNode(Simulator sim, RadioModel radioModel) {
		super(sim, radioModel);
	}
    
	/**
	 * Draws a filled circle, which is: <br>
	 *  - blue if the node is sending a message <br>
	 *  - red if the node received a corrupted message <br>
	 *  - green if the node received a message without problems <br>
	 *  - pink if the node sent a message <br>
	 *  - and black as a default<br>
	 * It also draws a line between mote and its parent, which is another mote
	 * who sent the message first to this.
	 */ 
	public void display(Display disp){
		Graphics g = disp.getGraphics();
		int      x = disp.x2ScreenX(this.x);
		int      y = disp.y2ScreenY(this.y);
               
		if( sending ){                    
			g.setColor( Color.blue );
		}else if( receiving ){
			if( corrupted )
				g.setColor( Color.red );
			else
				g.setColor( Color.green );                            
		}else{
			if( sent )
				g.setColor( Color.pink );
			else
				g.setColor( Color.black );
		}
		g.fillOval( x-3, y-3, 5, 5 );
		if( parent != null ){
			g.setColor( Color.black );
			int x1 = disp.x2ScreenX(parent.getX());
			int y1 = disp.y2ScreenY(parent.getY());
			g.drawLine(x,y,x1,y1);
		}
	}        

        public static void initialize(Node first) throws MatlabInvocationException
        {
            
           matrixRank.clear();
           System.out.println("Matrix Rank after clearing is : "+matrixRank.size());
           Node tempNode = first ;
		while (tempNode != null){
                        ((Mica2Node)tempNode).sending = false;
                        ((Mica2Node)tempNode).transmitting = false;
                        ((Mica2Node)tempNode).corrupted = false;
                        ((Mica2Node)tempNode).receiving = false;
                        ((Mica2Node)tempNode).sendingPostponed = false;
                        ((Mica2Node)tempNode).parentNode = null;
                        ((Mica2Node)tempNode).senderApplication = null;
                        ((Mica2Node)tempNode).messages.clear();
                        ((Mica2Node)tempNode).coeffs.clear();
                        Node.done = false;
                        
			matrixRank.add(1);
                        Integer nodeId = tempNode.id;
                        proxy.eval("message"+nodeId.toString()+" = []");
                        proxy.eval("messages"+nodeId.toString()+" = []");
                        proxy.eval("temp_messages"+nodeId.toString()+" = []");
                        proxy.eval("Coeffs"+nodeId.toString()+" = []");
                        proxy.eval("temp_Coeffs"+nodeId.toString()+" = []");
			tempNode = tempNode.nextNode;
		}
                System.out.println("Matrix Rank after initialization is : "+matrixRank.size());
        }
        public static void setMessages(Double quant, Node tempNode) throws MatlabInvocationException
        {
            quant = quant + 1;
            proxy.eval("eye(16)");
            Integer nodeId = tempNode.id;
            proxy.eval("message"+nodeId.toString() +" = A("+quant.toString()+")");
            proxy.eval("messages"+nodeId.toString()+" = "+"[messages"+nodeId.toString()+";message"+nodeId.toString()+"]");
            proxy.eval("temp_messages"+nodeId.toString()+" = "+"[temp_messages"+nodeId.toString()+";message"+nodeId.toString()+"]");
            proxy.eval("Coeffs"+nodeId.toString()+" = "+"[Coeffs"+nodeId.toString()+";A(2)*ans("+nodeId.toString()+",:)"+"]");
            proxy.eval("temp_Coeffs"+nodeId.toString()+" ="+"[temp_Coeffs"+nodeId.toString()+";A(2)*ans("+nodeId.toString()+",:)"+"]");
        }
	/**
	 * Starts up a simulator with a ROOT in the middle of a 300 by 300 meters
	 * field with 1000 motes and runs it in real time mode.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception, MatlabConnectionException, MatlabInvocationException{       
		
               
                //Create a proxy, which we will use to control MATLAB
                factory = new MatlabProxyFactory();
                proxy = factory.getProxy();
                LoggingMatlabProxy logger = new LoggingMatlabProxy(proxy);
                LoggingMatlabProxy.showInConsoleHandler();
                
                proxy.eval("cd C:\\Users\\daiict\\Desktop\\indoor_rssi_tracking\\fwdmatlabcode\\");
                proxy.eval("A =gf(0:255,8)");   
                System.out.println("creating nodes...");        
		Simulator sim = new Simulator();
		
		// creating the desired radio model, uncomment the one you need 
		//GaussianRadioModel radioModel = new GaussianRadioModel(sim);
		RayleighRadioModel radioModel = new RayleighRadioModel(sim);

		// creating the node in the middle of the field, and adding a broadcast
		// application
                long time0 = System.currentTimeMillis();
		TestBroadcastNode root = (TestBroadcastNode)sim.createNode( TestBroadcastNode.class, radioModel, 1, 0, 0, 0);
		BroadcastApplication bcApp = root.new BroadcastApplication(root);

                BroadcastApplication[] bcApps = new BroadcastApplication[16];
                
		// creating all the other nodes
		Node firstNode = sim.createNodes( TestBroadcastNode.class, radioModel, 2, 1000, 300, 5);
                //System.out.println("ID of firstNode is: "+firstNode.id);
                //initialize(firstNode,bcApps);
                Node tempNode = firstNode;
                int i = 0;
                proxy.eval("eye(16)");
                while(tempNode!=null)
                {
                    matrixRank.add(1);
                    bcApps[i] = ((TestBroadcastNode)tempNode).new BroadcastApplication(tempNode);
                    Integer nodeId = tempNode.id;
                    tempNode = tempNode.nextNode;
                    i++;
                }
		// This call is a must, please do not forget to call it whenever the mote 
		// field is set up
		radioModel.updateNeighborhoods();
        
        System.out.println("creation time: " + (System.currentTimeMillis()-time0) + " millisecs" );
        final long time1 = System.currentTimeMillis();
	         
	    System.out.println("start simulation");
                //System.out.println("Id of root is: "+root.id);	               
		//root.sendMessage(1, bcApp);
            
               tempNode = firstNode;
                int count = 1;
             /*
             while(tempNode!=null)
             {
                 //if(tempNode.id == 1)
                   //    break;
                 System.out.println("Message has been sent by node: "+ tempNode.id);
                 Integer nodeID = (Integer)tempNode.id;
                 Random randomGenerator = new Random();
                 Integer randomInt = randomGenerator.nextInt(256)+1;
                 //proxy.eval("temp"+nodeID.toString()+"=");
                 ArrayList temp = new ArrayList();
                 temp.add(randomInt);
                 tempNode.sendMessage(tempNode.id,temp, bcApps[count-1]);
                 count++;
                 tempNode = tempNode.nextNode;
             }
             */
             
                TargetTracking track = new TargetTracking();
                track.BeforeLoop(proxy);
                int no_of_targets=8;
                int no_of_motes = 16;
                int no_of_points = 100;  //no of points as mentioned in script_rssi_tracking.m
                int min = -45;
                int max = 0;
                double quantified;
                proxy.eval("sampling_epoch = 1");
                for(Integer temp=1; temp<=no_of_points; temp++)
                {
                    if(temp!=1)
                    {
                        proxy.eval("prev_obs_matrix = observation_matrix");
                        proxy.eval("curr_obs_matrix = observation_matrix"); 
                    }
                    track.LoopFirst(proxy);
                    if(temp==1)
                    {
                        proxy.eval("curr_obs_matrix = observation_matrix");
                    }
                    for(Integer temp2 = 1; temp2<=no_of_targets; temp2++)
                    {
                        //sim.renewSignal();
                        done = false;
                        System.out.println("Here ???");
                        initialize(firstNode);
                        for(Integer temp1=1; temp1<=no_of_motes; temp1++)
                        {
                            
                            double temp3 = ((double[]) proxy.returningEval("observation_matrix("+temp2.toString()+","+temp1.toString()+")", 1)[0])[0];
                            Double obs = (Double)temp3;
                            System.out.println("Obs by mote "+ temp1.toString() +" of target "+temp2.toString()+" is "+ obs);
                            if(obs>max || obs<min)
                            {
                                System.out.println("Obs value out of bounds.");
                                break;
                            }
                            else
                            {
                               quantified = floor((abs(obs-min)/(abs((max-min))/Math.pow(2,8))))+0; 
                               if(quantified>Math.pow(2, 8))
                               {
                                   System.out.println("Quantified value out of range. Equation wrong");
                                   break;
                               }
                               System.out.println("Quantified value is : "+quantified);
                               while(tempNode.id!=temp1 && tempNode!=null)
                               {
                                   tempNode = tempNode.nextNode;
                               }
                               
                               setMessages(quantified, tempNode);
                               tempNode = firstNode;
                            }
                        }
                        tempNode = firstNode;
                        int t =0;
                        while(tempNode!=null)
                        {
                            Random randomGenerator = new Random();
                               Integer randomInt = randomGenerator.nextInt(256)+1;
                               //proxy.eval("temp"+nodeID.toString()+"=");
                               ArrayList temp4 = new ArrayList();
                               temp4.add(randomInt);
                               tempNode.sendMessage(tempNode.id,temp4, bcApps[t]);
                               t++;
                               tempNode = tempNode.nextNode;
                        }
                        tempNode = firstNode;
                        sim.runWithDisplay();
                        tempNode = firstNode;
                        while(tempNode!=null)
                        {
                            proxy.eval("inv(temp_Coeffs"+tempNode.id+")");
                            proxy.eval("ans*temp_messages"+tempNode.id);
                            for(int l=1;l<=no_of_motes;l++)
                            {
                                proxy.eval("temp_val_gf = ans("+l+")");
                                proxy.eval("temp_val_double = double(temp_val_gf.x)");
                                proxy.eval("temp_obs = (temp_val_double*(abs((0-(-45)))/(2^8))) - 45");  // Remember to change 45 if min value changes
                                //proxy.eval("observation_matrix(l,"+tempNode.id+") = temp_obs");
                                proxy.eval("curr_obs_matrix("+temp2+","+l+") = temp_obs");
                            }
                            
                            tempNode = tempNode.nextNode;
                        }
                        track.LoopSecondPart1(proxy);
                        proxy.eval("timeSlots = "+Node.timeSlots);
                        track.LoopSecondPart2(proxy);
                        proxy.eval("sampling_epoch = sampling_epoch + 1");
                        tempNode = firstNode;
                        //if(temp2 == 1)
                        //        sim.runWithDisplayInRealTime();
                        /*
                        while(sim.eventQueue.size()!=0)
                        {
                            System.out.println("Waiting for prev values to get circulated.");
                        }
                        */
                        //Simulator.signal.await();
                    }
                }
                track.AfterLoop(proxy);
                 
        /*
        boolean realTime = true;
        if( realTime )
        {
            sim.runWithDisplayInRealTime();
        }
        else
        {
            // run as fast as possible and measure dump execution time
            Event event = new Event()
            {
                public void execute()
                {
                    System.out.println("execution time: " + (System.currentTimeMillis()-time1) + " millisecs" );
                }
            };
            event.time = Simulator.ONE_SECOND * 1000;
            sim.addEvent(event);
            sim.run(20000);
        } 
                */
        //System.out.println("here and there");
	}
}

