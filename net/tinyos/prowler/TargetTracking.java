/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tinyos.prowler;

import java.awt.Color;
import java.awt.Graphics;
import java.util.*;
import matlabcontrol.LoggingMatlabProxy;
import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.extensions.MatlabProxyLogger;
/**
 *
 * @author Shourya
 */
public class TargetTracking 
{
    public void BeforeLoop(MatlabProxy proxy) throws MatlabInvocationException
    {
        //proxy.eval("cd C:\\Users\\daiict\\Desktop\\indoor_rssi_tracking\\fwdmatlabcode\\");
        proxy.eval("a=0;\n" +
"b=20;\n" +
"no_of_targets=8;\n" +
"target_locn_index=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];\n" +
"%target_locn_index=[1 2 1 2 1 1 1 1 1 1 1 1 1 1 1 1];\n" +
"no_of_dimensions=3;\n" +
"no_of_particles=128;\n" +
"no_of_particles_1=512;\n" +
"no_of_cells=4;\n" +
"no_of_motes=16;\n" +
"T=1;\n" +
"sigma_sq_threhold_db=-10;\n" +
"sigma_sq_u_db=-35;\n" +
"sigma_w_db=1;\n" +
"epsilon_depend=0;\n" +
"vln=[0 0 1 1 1 2 1 2 2 0 0 1 1 1 2 1; 1 1 0 2 2 1 2 1 1 1 1 0 0 0 1 2; 2 2 1 1 1 0 1 0 0 2 2 1 1 1 0 1; 1 1 2 0 0 1 0 1 1 1 1 2 2 2 1 0];\n" +
"\n" +
"%etaln=10^(3/20)*randn(no_of_cells,no_of_motes);\n" +
"gamma_wall=-2;\n" +
"phi_nominal=5*ones(no_of_targets,1);\n" +
"  r(:,1) = [1; 0; 10];\n" +
"  r(:,2) = [9; 0; 10];\n" +
"  r(:,3) = [19; 0; 10];\n" +
"  r(:,4) = [1; 11; 10];\n" +
"  r(:,5) = [9; 11; 10];\n" +
"  r(:,6) = [19; 11; 10];\n" +
"  r(:,7) = [1; 19; 10];\n" +
"  r(:,8) = [11; 19; 10];\n" +
"  r(:,9) = [19; 19; 10];\n" +
"  r(:,10)= [1; 9; 10];\n" +
"  r(:,11)= [9; 9; 10];\n" +
"  r(:,12)= [11; 0; 10];\n" +
"  r(:,13)= [11; 9; 10];\n" +
"  r(:,14)= [19; 9; 10];\n" +
"  r(:,15)= [11; 11; 10];\n" +
"  r(:,16)= [9; 19; 10];\n" +
"\n" +
"\n" +
"%creation of static radio environment\n" +
"%static_phi_m_vec=10.^(phi_nominal*ones(no_of_targets,1)./10) + (10^(3/20)*randn(no_of_targets,1)).^2;\n" +
"static_phi_m_vec=phi_nominal.*ones(no_of_targets,1) + 3*randn(no_of_targets,1); \n" +
"for l=1:no_of_cells\n" +
"    for n=1:no_of_motes\n" +
"        if (l ~= target_locn(r(:,n)))\n" +
"            gamma_ln(l,n)=vln(l,n)*gamma_wall + 3*randn;\n" +
"            gamma_ln_initial_for_est(l,n)=vln(l,n)*gamma_wall;\n" +
"        else\n" +
"            %gamma_ln(l,n)=vln(l,n)*(10^(gamma_wall/10));\n" +
"            gamma_ln(l,n)=0;\n" +
"            gamma_ln_initial_for_est(l,n)=0;\n" +
"        end\n" +
"    end\n" +
"end\n" +
"\n" +
"gamma_ln_initial_for_est_vec=reshape(gamma_ln_initial_for_est',no_of_cells*no_of_motes,1);\n" +
"gamma_ln_vec=reshape(gamma_ln',no_of_cells*no_of_motes,1);\n" +
"gamma_ln_initial_for_est_vec_trimmed=[];\n" +
"gamma_ln_vec_trimmed=[];\n" +
"for l=1:length(gamma_ln_initial_for_est_vec)\n" +
"    if gamma_ln_initial_for_est_vec(l)~=0\n" +
"        gamma_ln_initial_for_est_vec_trimmed=[gamma_ln_initial_for_est_vec_trimmed;gamma_ln_initial_for_est_vec(l)];\n" +
"        gamma_ln_vec_trimmed=[gamma_ln_vec_trimmed;gamma_ln_vec(l)];\n" +
"    end\n" +
"end\n" +
"\n" +
"true_parameter_est=[static_phi_m_vec;gamma_ln_vec_trimmed];\n" +
"\n" +
"\n" +
"for p=1:no_of_particles_1\n" +
"    initial_state_vec=[];\n" +
"for m=1:no_of_targets\n" +
"    if m==1\n" +
"        %x_vel_initial=-0.5+rand;y_vel_initial=-0.5+rand;\n" +
"        x_vel_initial=0.5;y_vel_initial=0.5;\n" +
"        initial_state_vec=initial_target_location_velocity_vec(target_locn_index(m),x_vel_initial,y_vel_initial,no_of_dimensions);\n" +
"    else\n" +
"        %x_vel_initial=-0.5+rand;y_vel_initial=-0.5+rand;\n" +
"        x_vel_initial=0.5;y_vel_initial=0.5;\n" +
"        initial_state_vec=[initial_state_vec;initial_target_location_velocity_vec(target_locn_index(m),x_vel_initial,y_vel_initial,no_of_dimensions)];\n" +
"    \n" +
"    end\n" +
"end\n" +
"past_true_state_particle_matrix(:,:,p)=reshape(initial_state_vec,2*no_of_dimensions,no_of_targets);\n" +
"end\n" +
"\n" +
"\n" +
"\n" +
"no_of_points=10000;\n" +
"figure;hold on;\n" +
"% axis([0 20 0 20]);");
}
    public void LoopFirst(MatlabProxy proxy) throws MatlabInvocationException
    {
        proxy.eval("       if(sampling_epoch==1)\n" +
"state_vec=initial_state_vec;\n" +
"previous_filter_pdf_weights=(1/no_of_particles)*ones(no_of_targets,no_of_particles);\n" +
"\n" +
"%for ii=1:no_of_targets\n" +
"    %for jj=1:no_of_particles\n" +
"    %previous_zeta_matrix(:,:,ii)=a+(b-a)*rand(2*no_of_dimensions,no_of_particles);\n" +
"    %previous_zeta_matrix(:,:,ii)=1+6*rand(2*no_of_dimensions,no_of_particles)\n" +
"    %end\n" +
"%previous_state_estimate(ii,:)=compute_state_estimate(previous_zeta_matrix(:,:,ii));\n" +
"%end\n" +
"    \n" +
"for ii=1:no_of_targets\n" +
"    for jj=1:no_of_particles\n" +
"        %x_vel=-0.5+rand;y_vel=-0.5+rand;\n" +
"        x_vel=0.5;y_vel=0.5;\n" +
"        previous_zeta_matrix(:,jj,ii)=target_location_velocity_vec(target_locn_index(ii),x_vel,y_vel,3);\n" +
"    end\n" +
"       \n" +
"end\n" +
"       else  \n" +
"next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_u_db,state_vec,epsilon_depend);\n" +
"next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,state_vec,T,sigma_sq_u_db,epsilon_depend);\n" +
"state_vec = next_state_vec;\n" +
"       end\n" +
"       target_state_matrix=reshape(state_vec,2*no_of_dimensions,no_of_targets);\n" +
"       observation_matrix=obs_model(state_vec,no_of_dimensions,no_of_targets,no_of_motes,static_phi_m_vec,gamma_ln,r,sigma_w_db);");
    }
    
    public void LoopSecondPart1(MatlabProxy proxy) throws MatlabInvocationException
    {
        proxy.eval("for pp=1:no_of_particles\n" +
"          previous_zeta_vec=reshape(previous_zeta_matrix(:,pp,:),2*no_of_dimensions*no_of_targets,1);\n" +
"          next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_threhold_db,previous_zeta_vec,0);\n" +
"          next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,previous_zeta_vec,T,sigma_sq_threhold_db,0);\n" +
"          current_zeta_matrix(:,pp,:)=reshape(next_state_vec,2*no_of_dimensions,no_of_targets);         \n" +
"      end\n" +
"      %compute particle weights and do the resampling\n" +
"       for i=1:no_of_targets\n" +
"           unnormalized_weights(i,:)=compute_particle_weights_2(curr_obs_matrix(i,:),current_zeta_matrix(:,:,i),static_phi_m_vec(i),gamma_ln,1,no_of_targets,r,sigma_w_db);\n" +
"           for j=1:no_of_particles\n" +
"               %resampled_particle_indices(i,:)=randsample(no_of_particles,no_of_particles,true,unnormalized_weights(i,:));\n" +
"               resampled_particle_indices(i,j)=acc_rej(unnormalized_weights(i,:)/(sum(unnormalized_weights(i,:))));\n" +
"           end\n" +
"       end");
    }
    
    public void LoopSecondPart2(MatlabProxy proxy) throws MatlabInvocationException
    {
        proxy.eval("for i=1:no_of_targets\n" +
"           temp_vec=zeros(2*no_of_dimensions,1);\n" +
"           for p=1:length(resampled_particle_indices(i,:))\n" +
"               temp_vec=temp_vec+current_zeta_matrix(:,resampled_particle_indices(i,p),i);\n" +
"               temp_current_zeta_matrix(:,p,i)=current_zeta_matrix(:,resampled_particle_indices(i,p),i);\n" +
"           end\n" +
"           current_state_estimate(:,i)=(1/no_of_particles)*(temp_vec);\n" +
"           est_posn_vec(:,i)=extract_posn_vect(current_state_estimate(:,i),3,1);\n" +
"           true_posn_vec(:,i)=extract_posn_vect(target_state_matrix(:,i),3,1);           \n" +
"           targetposn_errorsq_matrix(i,sampling_epoch)=norm(([true_posn_vec(1,i);true_posn_vec(2,i)]-[est_posn_vec(1,i);est_posn_vec(2,i)]),2)^2;\n" +
"           target_mse(i)=(1/timeSlots)*sum(targetposn_errorsq_matrix(i,:));\n" +
"       end\n" +
"       rms_target_error=sqrt((1/no_of_targets)*sum(target_mse));\n" +
"       plot(timeSlots,rms_target_error,'o','MarkerSize',5);\n" +
"       drawnow;\n" +
"       current_zeta_matrix=temp_current_zeta_matrix;\n" +
"       previous_zeta_matrix=current_zeta_matrix;\n" +
"       if (sampling_epoch > 9500)\n" +
"%            plot(true_posn_vec(1,1),true_posn_vec(2,1),'o','MarkerSize',5);\n" +
"%            plot(est_posn_vec(1,1),est_posn_vec(2,1),'ro','MarkerSize',5);\n" +
"%            drawnow;\n" +
"           true_posn_vec_each_instant(:,sampling_epoch)=[true_posn_vec(1,1);true_posn_vec(2,1)];\n" +
"           est_posn_vec_each_instant(:,sampling_epoch)=[est_posn_vec(1,1);est_posn_vec(2,1)];\n" +
"         end\n" +
"     \n" +
"%             plot(true_posn_vec(1,1),true_posn_vec(2,1),'o','MarkerSize',5);\n" +
"%             plot(est_posn_vec(1,1),est_posn_vec(2,1),'ro','MarkerSize',5);\n" +
"%             drawnow;");
    }
    /*
    public void LoopSecondPart(MatlabProxy proxy) throws MatlabInvocationException
    {
        proxy.eval("for pp=1:no_of_particles\n" +
"          previous_zeta_vec=reshape(previous_zeta_matrix(:,pp,:),2*no_of_dimensions*no_of_targets,1);\n" +
"          next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_threhold_db,previous_zeta_vec,0);\n" +
"          next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,previous_zeta_vec,T,sigma_sq_threhold_db,0);\n" +
"          current_zeta_matrix(:,pp,:)=reshape(next_state_vec,2*no_of_dimensions,no_of_targets);         \n" +
"      end\n" +
"      %compute particle weights and do the resampling\n" +
"       for i=1:no_of_targets\n" +
"           unnormalized_weights(i,:)=compute_particle_weights_2(observation_matrix(i,:),current_zeta_matrix(:,:,i),static_phi_m_vec(i),gamma_ln,1,no_of_targets,r,sigma_w_db);\n" +
"           for j=1:no_of_particles\n" +
"               %resampled_particle_indices(i,:)=randsample(no_of_particles,no_of_particles,true,unnormalized_weights(i,:));\n" +
"               resampled_particle_indices(i,j)=acc_rej(unnormalized_weights(i,:)/(sum(unnormalized_weights(i,:))));\n" +
"           end\n" +
"       end\n" +
"       for i=1:no_of_targets\n" +
"           temp_vec=zeros(2*no_of_dimensions,1);\n" +
"           for p=1:length(resampled_particle_indices(i,:))\n" +
"               temp_vec=temp_vec+current_zeta_matrix(:,resampled_particle_indices(i,p),i);\n" +
"               temp_current_zeta_matrix(:,p,i)=current_zeta_matrix(:,resampled_particle_indices(i,p),i);\n" +
"           end\n" +
"           current_state_estimate(:,i)=(1/no_of_particles)*(temp_vec);\n" +
"           est_posn_vec(:,i)=extract_posn_vect(current_state_estimate(:,i),3,1);\n" +
"           true_posn_vec(:,i)=extract_posn_vect(target_state_matrix(:,i),3,1);           \n" +
"           targetposn_errorsq_matrix(i,sampling_epoch)=norm(([true_posn_vec(1,i);true_posn_vec(2,i)]-[est_posn_vec(1,i);est_posn_vec(2,i)]),2)^2;\n" +
"       end\n" +
"       current_zeta_matrix=temp_current_zeta_matrix;\n" +
"       previous_zeta_matrix=current_zeta_matrix;\n" +
"       if (sampling_epoch > 9500)\n" +
"           plot(true_posn_vec(1,1),true_posn_vec(2,1),'o','MarkerSize',5);\n" +
"           plot(est_posn_vec(1,1),est_posn_vec(2,1),'ro','MarkerSize',5);\n" +
"           drawnow;\n" +
"           true_posn_vec_each_instant(:,sampling_epoch)=[true_posn_vec(1,1);true_posn_vec(2,1)];\n" +
"           est_posn_vec_each_instant(:,sampling_epoch)=[est_posn_vec(1,1);est_posn_vec(2,1)];\n" +
"         end\n" +
"     \n" +
"            plot(true_posn_vec(1,1),true_posn_vec(2,1),'o','MarkerSize',5);\n" +
"            plot(est_posn_vec(1,1),est_posn_vec(2,1),'ro','MarkerSize',5);\n" +
"            drawnow;");
    }
    */
    public void AfterLoop(MatlabProxy proxy) throws MatlabInvocationException
    {
        proxy.eval("for i=1:no_of_targets\n" +
"    target_mse(i)=(1/no_of_points)*sum(targetposn_errorsq_matrix(i,:));\n" +
"end\n" +
"rms_tracking_error=sqrt((1/no_of_targets)*sum(target_mse));\n" +
"%figure(1);");
    }
}
