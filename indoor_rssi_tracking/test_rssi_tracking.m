function[z_next]=test_rssi_tracking(no_of_targets,no_of_dimensions,T,previous_state_vec)


%creating the block diagonal A matrix;
c=[1 T;0 1];
%A_mat=cell(1,no_of_targets*no_of_dimensions);
%[A_mat{:}]=deal(sparse(c));
%A=full(blkdiag(A_mat{:}));
A=kron(eye(no_of_targets*no_of_dimensions),c)
B=kron(eye(no_of_targets*no_of_dimensions),[T^2/2, T/2]')
u=sqrt(10^(-3.5))*randn(no_of_targets*no_of_dimensions,1);
z_next=A*previous_state_vec + B*u;

