%function[true_state_particles_matrix]=generation_true_state_particles(past_state_particle_matrix,no_of_particles,no_of_targets,no_of_dimensions,T,proc_noise_var_db,epsilon_depend)

function[true_state_particles_matrix]=generation_true_state_particles(no_of_particles,no_of_targets,no_of_dimensions,a,b,x_vel,y_vel)
%this function generates true state particles given the past true state
%particles. This particles are required in the computation of the Hessian
%matrix, used in the stochastic recursion.

% c=[1 T;0 1];
% A=kron(eye(no_of_targets*no_of_dimensions),c);
% B=kron(eye(no_of_targets*no_of_dimensions),[T^2/2, T/2]');

% for p=1:no_of_particles
%     previous_state_vec=reshape(past_state_particle_matrix(:,:,p),2*no_of_targets*no_of_dimensions,1);
%     particle_weights(p)=A*previous_state_vec + (10^(proc_noise_var_db/10))*B*eye(no_of_targets*no_of_dimensions)*B';
% end
% particle_weights=particle_weights/(sum(particle_weights));

for p=1:no_of_particles
    state_vec=[];
for m=1:no_of_targets
    if m==1                
        state_vec=[a+(b-a)*rand;x_vel;a+(b-a)*rand;y_vel;0;0];
    else        
        state_vec=[state_vec;a+(b-a)*rand;x_vel;a+(b-a)*rand;y_vel;0;0];
    
    end
end
true_state_particles_matrix(:,:,p)=reshape(state_vec,2*no_of_dimensions,no_of_targets);
end

% for p=1:no_of_particles
%     previous_state_vec=reshape(past_state_particle_matrix(:,:,p),2*no_of_targets*no_of_dimensions,1);
%     for m=1:no_of_targets
%         next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,proc_noise_var_db,previous_state_vec,epsilon_depend);
%         checked_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,previous_state_vec,T,proc_noise_var_db,epsilon_depend);
%         true_state_particles_matrix(:,:,p)=reshape(checked_state_vec,2*no_of_dimensions,no_of_targets);
%         
%     end
% end