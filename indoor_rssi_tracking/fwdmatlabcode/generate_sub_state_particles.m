function [sub_state_part_matrix,initial_state_est]=generate_sub_state_particles(no_of_particles,a,b,no_of_dimensions)

% for i=1:no_of_particles
%     sub_state_part_matrix(:,i)=a+(b-a)*rand(2*no_of_dimensions,1);
% end

for j=1:2*no_of_dimensions
initial_state_est(j)=(sum(sub_state_part_matrix(j,:)))/(length(sub_state_part_matrix));
end
