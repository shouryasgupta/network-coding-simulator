function[gamma_l_vec]=score_of_gammaln(zeta_matrix_of_given_target,obs_vector_of_given_target,cell_index, phi,gamma_l_previous_vec,mote_locn_vec,likelihood_value_given_parameters)
[no_of_rows_of_zeta,no_of_particles]=size(zeta_matrix_of_given_target);
[no_of_rows_obs_vec,no_of_motes]=size(obs_vector_of_given_target);

for j=1:no_of_particles
    pos_vect=extract_posn_vect(zeta_matrix_of_given_target(:,j),3,1);
    pos_vect(3,:)=0;
    cell_locn=target_locn(pos_vect);
        if (cell_locn ~=cell_index)
            temp_gamma_l_vec(:,j)=zeros(no_of_motes,1);
        end
        
    for i=1:no_of_motes
        dist_mote_target=sum((pos_vect - mote_locn_vec(:,i)).^2)^(0.5);              
        temp_gamma_l_vec(i,j)=(obs_vector_of_given_target(i) -phi - gamma_l_previous_vec(i) + 20*log10(dist_mote_target))*likelihood_value_given_parameters(j);         
    end    
end

for i=1:no_of_motes
    gamma_l_vec(i)=(1/no_of_particles)*sum(temp_gamma_l_vec(i,:));
end