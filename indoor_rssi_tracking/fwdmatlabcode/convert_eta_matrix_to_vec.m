function[total_state_vec_1]=convert_eta_matrix_to_vec(eta_matrix)

[no_of_targets,length_of_state_vec]=size(eta_matrix);
% for i=1:no_of_targets
%     if i==1
%     first_sub_vector=eta_matrix(i,:);
%     total_state_vec=first_sub_vector;
%     else
%         total_state_vec=[first_sub_vector eta_matrix(i,:)];
%     end
% end
% total_state_vec_1=total_state_vec';

total_state_vec_1=reshape(eta_matrix',1,no_of_targets*length_of_state_vec)';