function[cell_no]=target_locn(posn_vec)
x_comp_posn_vec=posn_vec(1);
y_comp_posn_vec=posn_vec(2);
if(x_comp_posn_vec< 10)
    if(y_comp_posn_vec<10)
        cell_no=1;
    else
        cell_no=4;
    end
else
    if(y_comp_posn_vec<10)
        cell_no=2;
    else
        cell_no=3;
    end
end
