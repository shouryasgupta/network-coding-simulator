function[observation_matrix]=obs_model(state_vec,no_of_dimensions,no_of_targets,no_of_motes,phi_m_vec,gamma_ln_matrix,mote_locn_vec,sigma_w_db)

  
xn=extract_posn_vect(state_vec,no_of_dimensions,no_of_targets);
xn(3,:)=0;

for m=1:no_of_targets
    for n=1:no_of_motes
        dist_bw_target_mote=sum((xn(:,m)-mote_locn_vec(:,n)).^2)^(0.5);
        %observation_matrix(m,n)=sqrt((phi_m_vec(m)*gamma_ln_matrix(target_locn(xn(:,m)),n)/(dist_bw_target_mote)^2)) + (sigma_w*randn);
        observation_matrix(m,n)=phi_m_vec(m) + gamma_ln_matrix(target_locn(xn(:,m)),n) - (2*10*log10(dist_bw_target_mote)) + ((sigma_w_db)*randn);
    end
end






% for j=1:no_of_targets
% % hold on;
% % plot(xn(1,j),xn(2,j),'.r','MarkerSize',5);
% % hold off;
% % fi_m=((10^0.3))*randn(3,1);
% % fi_final=fi_m+fi_nomial;
% % wmn=10^.2*randn(3,1);
% 
% cell(j)=target_locn(xn(:,j));
% 
% 
%  for i=1:1:no_of_motes
%      
%      a=sum((xn(:,j)-r(:,i)).^2).^0.5;
%      fi_m=((10^0.3))*randn(1,1);
%      fi_final=fi_m+fi_nomial(j);
%      wmn=10^.2*randn(1,1);
%      b= fi_final*(1/(a^2));
% 
%            if(mote_loc(i)== cell(j))
%           Iln=1;
%       else
%           Iln=0;
%       end
%       nln=10^.3*randn;
%       
%       gama_ln=(vln(cell(j),i)*gama_wall)+(nln*(1-Iln));
%       Ymnt(j,i)=gama_ln*b+wmn;
%  end
% 
% end

