function [state_est]=compute_state_estimate(zeta_matrix)

% for i=1:no_of_particles
%     sub_state_part_matrix(:,i)=a+(b-a)*rand(2*no_of_dimensions,1);
% end
[length_of_state_vec,no_of_particles]=size(zeta_matrix);

for j=1:length_of_state_vec
state_est(j)=(sum(zeta_matrix(j,:)))/(no_of_particles);
end
