function [score_fn_gradient]=computation_of_gradient_of_scorefn(observation_matrix,zeta_matrix,power_est_vec,gamma_est_matrix,no_of_particles,mote_locn_vec,sigma_w)

[no_of_targets,no_of_motes]=size(observation_matrix);
[no_of_cells,no_of_motes]=size(gamma_est_matrix);


for i=1:no_of_targets
    for j=1:no_of_particles
        temp=0;temp1=0;
        position_vec=extract_posn_vect(zeta_matrix(:,j,i),3,1);
        position_vec(3,:)=0;
        for k=1:no_of_motes
            dist_mote_target=sum((position_vec - mote_locn_vec(:,k)).^2)^(0.5);
            some_thing=(observation_matrix(i,k)-power_est_vec(i)-gamma_est_matrix(target_locn(position_vec),k)+20*log10(dist_mote_target));
            some_thing_sq=some_thing^(2);
            temp=temp + some_thing;
            temp1=temp1+some_thing_sq;
        end
        temp2(j,i)=(2*pi*sigma_w^2)^(-no_of_motes/2)*exp(-(1/(2*sigma_w^2))*temp1);
        G(j,i)=temp2(j,i)*temp;
        H(j,i)=sigma_w^2*temp2(j,i);
    end
   D_m(i)=(1/no_of_particles)*sum(H(:,i));
   score_phi_nr(i)=(1/no_of_particles)*sum(G(:,i));
   score_phi(i)=score_phi_nr(i)/D_m(i);
end



for l=1:no_of_cells
    for m=1:no_of_targets
            gamma_grad(l,:,m)=(1/D_m(m))*score_of_gammaln(zeta_matrix(:,:,m),observation_matrix(m,:),l,power_est_vec(m),gamma_est_matrix(l,:),mote_locn_vec,temp2(:,m));
        end
end
    score_gamma_grad=zeros(no_of_cells,no_of_motes);
for m=1:no_of_targets
    score_gamma_grad=score_gamma_grad + gamma_grad(:,:,m);
end
score_gamma_ln_vec=reshape(score_gamma_grad',1,no_of_cells*no_of_motes)';
score_fn_gradient=[score_phi';score_gamma_ln_vec];
