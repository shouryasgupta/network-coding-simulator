function[target_neighbourhood_matrix] = compute_target_neighbourhood(aggr_radius,current_owner_mote,mote_location)
[no_of_dimensions, no_of_motes]=size(mote_location);
[owner,no_of_targets]= size(current_owner_mote);

target_neighbourhood_matrix=zeros(no_of_targets,no_of_motes);

for m = 1:no_of_targets
    owner_for_current_target = current_owner_mote(m);
    l=1;
for n = 1: no_of_motes
    mote_location(3,n)=0;
    mote_location(3,owner_for_current_target)=0;
    d = norm((mote_location(:,owner_for_current_target)-mote_location(:,n)),2);
    if (d <= aggr_radius)
        target_neighbourhood_matrix(m,l) = n;
        l=l+1;
    end
end
% if (max(size(neighbourhood_matrix)) < )            % -- mote 61-(0,0) covers max 13 motes including itself...and so the other motes too
%     neighbourhood_matrix = padarray(neighbourhood_matrix, 13-max(size(neighbourhood_matrix)),'post');
% end
% target_neighbourhood_matrix = [target_neighbourhood_matrix,neighbourhood_matrix];
end


    
    
    