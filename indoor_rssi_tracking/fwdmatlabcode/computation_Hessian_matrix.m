function[Hessian_matrix]=computation_Hessian_matrix(no_of_motes,no_of_cells,no_of_targets,sigma_w_db,true_state_particles_matrix,no_of_particles,vln)

block_matrix_phi_m=(no_of_motes/(sigma_w_db^2))*eye(no_of_targets);

index_gamma_ln_particles_vec=1;
for l=1:no_of_cells
    for n=1:no_of_motes
        if vln(l,n)~=0
            for p=1:no_of_particles
                temp=0;
                for m=1:no_of_targets
                    pos_vect=extract_posn_vect(true_state_particles_matrix(:,m,p),3,1);
                    pos_vect(3,:)=0;
                    cell_locn=target_locn(pos_vect);
                    if cell_locn ==l
                        temp=temp+1;
                    end
                end
            gamma_ln_particles_vec(index_gamma_ln_particles_vec,p)=temp;
            
            end
        index_gamma_ln_particles_vec=index_gamma_ln_particles_vec+1;
    end
    end
end

%gamma_ln_vec=[];
for l=1:length(gamma_ln_particles_vec(:,1))
    gamma_ln_vec(l)=(1/(no_of_particles))*sum(gamma_ln_particles_vec(l,:));
end


% for i=1:no_of_cells*no_of_motes
%     if (rem(i,no_of_motes)==0)
%         cell_position_val(i)=i/no_of_motes;
%     else
%         cell_position_val(i)=floor(i/no_of_motes)+1;
%     end
% end
% for p=1:no_of_particles
%     temp=0;
%     for l=1:no_of_motes*no_of_cells
%         for m=1:no_of_targets
%             position_vec=extract_posn_vect(true_state_particles_matrix(:,m,p),3,1);
%             position_vec(3,:)=0;
%             cell_locn=target_locn(position_vec);
%             if (cell_locn ==cell_position_val(l))
%                 temp=temp+1;
%             end
%         
%         end
%     cell_index_matrix_1(l,p)=temp;
%     end
% end
% 
% for i=1:no_of_cells*no_of_motes
%     cell_index_vec1(i)=(1/no_of_particles)*sum(cell_index_matrix_1(i,:));
% end
% 
% block_matrix_gamma_ln=((1/sigma_w_db)^2)*diag(cell_index_vec1);
block_matrix_gamma_ln=((1/sigma_w_db)^2)*diag(gamma_ln_vec);

block_matrix_phim_gammaln_particles=zeros(no_of_targets,length(gamma_ln_vec),no_of_particles);
for l=1:no_of_cells
    for n=1:no_of_motes
        if vln(l,n)~=0
            for m=1:no_of_targets
                for q=1:length(gamma_ln_vec)
                for p=1:no_of_particles
                    pos_vect=extract_posn_vect(true_state_particles_matrix(:,m,p),3,1);
                    pos_vect(3,:)=0;
                    cell_locn=target_locn(pos_vect);
                    if cell_locn ==l
                    block_matrix_phim_gammaln_particles(m,q,p)=block_matrix_phim_gammaln_particles(m,q,p) +1;
                    end
                end
            end
        end
    end
    end
end
block_matrix_phim_gammaln=zeros(no_of_targets,length(gamma_ln_vec));
%size(block_matrix_phim_gammaln)
for p=1:no_of_particles
    block_matrix_phim_gammaln=block_matrix_phim_gammaln + block_matrix_phim_gammaln_particles(:,:,p);
end
block_matrix_phim_gammaln=((1/sigma_w_db)^2)*(1/no_of_particles)*block_matrix_phim_gammaln;

% block_matrix_phim_gammaln=zeros(no_of_targets,no_of_motes*no_of_cells);
% for p=1:no_of_particles
%     cell_index_matrix_2=zeros(no_of_targets,no_of_motes*no_of_cells);
%     for m=1:no_of_targets
%         for l=1:no_of_cells*no_of_motes
%             position_vec=extract_posn_vect(true_state_particles_matrix(:,m,p),3,1);
%             position_vec(3,:)=0;
%             cell_locn=target_locn(position_vec);
%             if (cell_locn ==cell_position_val(l))
%             cell_index_matrix_2(m,l)=cell_index_matrix_2(m,l)+1;
%             end
%         end
%     end
%     block_matrix_phim_gammaln=block_matrix_phim_gammaln + cell_index_matrix_2;
% end
%block_matrix_phim_gammaln=((1/sigma_w_db)^2)*(1/no_of_particles)*block_matrix_phim_gammaln;

%creation of Hessian matrix
Hessian_matrix_1=horzcat(block_matrix_phi_m,block_matrix_phim_gammaln);
Hessian_matrix_2=horzcat(block_matrix_phim_gammaln',block_matrix_gamma_ln);
Hessian_matrix=vertcat(Hessian_matrix_1,Hessian_matrix_2);



