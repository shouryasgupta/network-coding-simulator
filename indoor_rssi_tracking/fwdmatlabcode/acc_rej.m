% To generate using acceptance rejection method
function y=acc_rej(pi0)
pi_max=max(pi0);
N=length(pi0);
x=floor(N*rand)+1;
c=pi_max/(1/N);
u1=2;
while u1 > pi0(x)/(c*(1/N))
    x=floor(N*rand)+1;
    u1=rand;
end
y=x;
