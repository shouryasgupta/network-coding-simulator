function[xn]=extract_posn_vect(state_vec,no_of_dimensions,no_of_targets)  

xn=zeros(no_of_dimensions,no_of_targets);
col=1;
row=1;
for i=1:no_of_dimensions*no_of_targets
    %if((mod(i,no_of_dimensions))== 0)
    %    col=col+1;
    %end
    xn(row,col)=state_vec(2*i-1);
    if((mod(i,no_of_dimensions))== 0)
        row=0;
        col=col+1;
    end
    row=row+1;
end
%xn(no_of_dimensions)=0;