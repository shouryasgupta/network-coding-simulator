function[predictor_pdf_particles_matrix]=generation_predictor_pdf_particles(no_of_particles,no_of_targets,no_of_dimensions,T,proc_noise_var_db,epsilon_depend,past_state_particle_matrix,resampled_particle_indices)
%this function generates predictor pdf particles given the filter pdf
%weights.

c=[1 T;0 1];
A=kron(eye(no_of_targets*no_of_dimensions),c);
B=kron(eye(no_of_targets*no_of_dimensions),[T^2/2, T/2]');

for p=1:no_of_particles
    previous_state_vec=[];
    for t=1:no_of_targets
        if t==1
            previous_state_vec=past_state_particle_matrix(:,resampled_particle_indices(t,p),t);
        else
    previous_state_vec=[previous_state_vec;past_state_particle_matrix(:,resampled_particle_indices(t,p),t)];
        end
        end
    predictor_pdf_particles_vec=A*previous_state_vec + (10^(proc_noise_var_db/10))*B*randn(no_of_targets*no_of_dimensions,1);
    predictor_pdf_particles_vec=check_target_state_vec(predictor_pdf_particles_vec,no_of_dimensions,no_of_targets,previous_state_vec,T,proc_noise_var_db,epsilon_depend);
    predictor_pdf_particles_matrix(:,p,:)=reshape(predictor_pdf_particles_vec,2*no_of_dimensions,no_of_targets);    
end
