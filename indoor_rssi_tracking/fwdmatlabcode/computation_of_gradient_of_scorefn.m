function [score_fn_gradient]=computation_of_gradient_of_scorefn(observation_matrix,zeta_matrix,power_est_vec,gamma_est_matrix,no_of_particles,mote_locn_vec,sigma_w,vln)

[no_of_targets,no_of_motes]=size(observation_matrix);
[no_of_cells,no_of_motes]=size(gamma_est_matrix);

%power_est_vec=parameter_est_vec(1:no_of_targets);


for i=1:no_of_targets
    for j=1:no_of_particles
        temp=0;temp1=0;
        position_vec=extract_posn_vect(zeta_matrix(:,j,i),3,1);
        position_vec(3,:)=0;
        for k=1:no_of_motes
            dist_mote_target=sum((position_vec - mote_locn_vec(:,k)).^2)^(0.5);
            some_thing=(observation_matrix(i,k)-power_est_vec(i)-gamma_est_matrix(target_locn(position_vec),k)+20*log10(dist_mote_target));
            some_thing_sq=some_thing^(2);
            temp=temp + some_thing;
            temp1=temp1+some_thing_sq;
        end
        temp2(j,i)=(2*pi*sigma_w^2)^(-no_of_motes/2)*exp(-(1/(2*sigma_w^2))*temp1);
        G(j,i)=temp2(j,i)*temp;
        H(j,i)=sigma_w^2*temp2(j,i);
    end
   D_m(i)=(1/no_of_particles)*sum(H(:,i));
   score_phi_nr(i)=(1/no_of_particles)*sum(G(:,i));
   score_phi(i)=score_phi_nr(i)/D_m(i);
end

cell_index_vec=zeros(no_of_cells,no_of_particles,no_of_targets);
for m=1:no_of_targets
for l=1:no_of_cells
    for j=1:no_of_particles
   pos_vect=extract_posn_vect(zeta_matrix(:,j,m),3,1);
    pos_vect(3,:)=0;
    cell_locn=target_locn(pos_vect);
        if (cell_locn ==l)
            cell_index_vec(l,j,m)=1;
        end     
    end
end
end

for m=1:no_of_targets
    for l=1:no_of_cells
        for n=1:no_of_motes
            temp=0;
    for p=1:no_of_particles
        pos_vect=extract_posn_vect(zeta_matrix(:,p,m),3,1);
        pos_vect(3,:)=0;
        %the line below was changed on 18/03/2013: G(p,m) changed to     
        %temp2(p,m)
        temp=temp + temp2(p,m)*(observation_matrix(m,n)-power_est_vec(m) - gamma_est_matrix(target_locn(pos_vect),n)+ 20*log10(norm(pos_vect-mote_locn_vec(:,n),2)))*cell_index_vec(l,p,m);
    end
    temp_score_gamma_grad(l,n,m)=(1/no_of_particles)*temp;
        end
    end
end

score_gamma_grad=zeros(no_of_cells,no_of_motes);
for m=1:no_of_targets
    score_gamma_grad=score_gamma_grad + temp_score_gamma_grad(:,:,m)./D_m(m);
end

% for l=1:no_of_cells
%     for n=1:no_of_motes
%         for m=1:no_of_targets
%             %[row1,col1]=size(G(:,m))
%             %[row2,col2]=size(cell_index_vec(l,:,m))
%             score_gamma_grad_term1(:,m)=G(:,m)'.*cell_index_vec(l,:,m);
%             score_gamma_grad_eachtarget(m)=(1/no_of_particles)*(sum(score_gamma_grad_term1(:,m))/D_m(m));
%         end
%         score_gamma_grad(l,n)=sum(score_gamma_grad_eachtarget);
%     end
% end



% for l=1:no_of_cells
%     for m=1:no_of_targets
%             gamma_grad(l,:,m)=(1/D_m(m))*score_of_gammaln(zeta_matrix(:,:,m),observation_matrix(m,:),l,power_est_vec(m),gamma_est_matrix(l,:),mote_locn_vec,temp2(:,m));
%         end
% end
%     score_gamma_grad=zeros(no_of_cells,no_of_motes);
% for m=1:no_of_targets
%     score_gamma_grad=score_gamma_grad + gamma_grad(:,:,m);
% end

%score_gamma_ln_vec=reshape(score_gamma_grad',1,no_of_cells*no_of_motes)';
score_gamma_ln_vec=[];
for l=1:no_of_cells
    for n=1:no_of_motes
        if vln(l,n)~=0
            score_gamma_ln_vec=[score_gamma_ln_vec;score_gamma_grad(l,n)];
        end
    end
end
score_fn_gradient=[score_phi';score_gamma_ln_vec];
