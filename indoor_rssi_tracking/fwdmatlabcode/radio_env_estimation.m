%function[current_parameter_est,previous_power_est,previous_gamma_est_matrix,current_zeta_matrix,resampled_particle_indices]=radio_env_estimation(previous_power_est,previous_gamma_est_matrix,previous_observation_matrix,observation_matrix,previous_zeta_matrix,resampled_particle_indices,gamma_ln_initial_for_est_vec_trimmed,fisher_info_matrix,T,sigma_sq_threhold_db,r,sigma_w_db,vln,step_size,algorithm_type)
function[current_parameter_est,previous_power_est,previous_gamma_est_matrix,current_zeta_matrix]=radio_env_estimation(previous_power_est,previous_gamma_est_matrix,previous_observation_matrix,observation_matrix,previous_zeta_matrix,gamma_ln_initial_for_est_vec_trimmed,fisher_info_matrix,T,sigma_sq_threhold_db,r,sigma_w_db,vln,step_size,algorithm_type)

       previous_parameter_est=[previous_power_est;gamma_ln_initial_for_est_vec_trimmed];
       [no_of_cells,no_of_motes]=size(vln);
       [no_of_rows_zeta_matrix,no_of_particles,no_of_targets]=size(previous_zeta_matrix);
       no_of_dimensions=no_of_rows_zeta_matrix/2;
       
       [score_fn_grad]=computation_of_gradient_of_scorefn(previous_observation_matrix,previous_zeta_matrix,previous_power_est,previous_gamma_est_matrix,no_of_particles,r,sigma_w_db,vln);
       if (strcmp(algorithm_type,'natural'))
       current_parameter_est=previous_parameter_est + step_size*pinv(fisher_info_matrix)*score_fn_grad;
       end
       if (strcmp(algorithm_type,'regular'))
       %current_parameter_est=previous_parameter_est + norm(pinv(fisher_info_matrix),'fro')*step_size*eye(length(previous_parameter_est))*score_fn_grad;
       current_parameter_est=previous_parameter_est + step_size*eye(length(previous_parameter_est))*score_fn_grad;
       end
       if (strcmp(algorithm_type,'new'))
           predictor_pdf_particles_matrix=generation_predictor_pdf_particles(no_of_particles,no_of_targets,no_of_dimensions,T,sigma_sq_threhold_db,0,previous_zeta_matrix,resampled_particle_indices);
           [score_fn_grad_using_predictor_pdf]=computation_of_gradient_of_scorefn(previous_observation_matrix,predictor_pdf_particles_matrix,previous_power_est,previous_gamma_est_matrix,no_of_particles,r,sigma_w_db,vln);
           current_parameter_est=previous_parameter_est + step_size*eye(length(previous_parameter_est))*score_fn_grad_using_predictor_pdf;
        end
       
       previous_gamma_est_matrix_vec=current_parameter_est(no_of_targets+1:length(current_parameter_est));
       previous_gamma_est_matrix_temp=zeros(no_of_cells,no_of_motes);
       index_parameter_est_vec=1;
       for i=1:no_of_cells
           for j=1:no_of_motes
               if vln(i,j)~=0
                   previous_gamma_est_matrix_temp(i,j)=previous_gamma_est_matrix_vec(index_parameter_est_vec);
                   index_parameter_est_vec=index_parameter_est_vec+1;
               end
               end
           end
              previous_power_est=current_parameter_est(1:no_of_targets);previous_gamma_est_matrix=previous_gamma_est_matrix_temp;
              
      %generation of particles for the current epoch for each target using
      %the kinematic prior
      for pp=1:no_of_particles
          previous_zeta_vec=reshape(previous_zeta_matrix(:,pp,:),2*no_of_dimensions*no_of_targets,1);
          %the 2 lines immediately below were added on 27/03/2013
          next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_threhold_db,previous_zeta_vec,0);
          next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,previous_zeta_vec,T,sigma_sq_threhold_db,0);
          current_zeta_matrix(:,pp,:)=reshape(next_state_vec,2*no_of_dimensions,no_of_targets);
      end
      %compute particle weights and do the resampling
       for i=1:no_of_targets
           unnormalized_weights(i,:)=compute_particle_weights_1(observation_matrix(i,:),current_zeta_matrix(:,:,i),previous_power_est(i),previous_gamma_est_matrix,1,no_of_targets,r,sigma_w_db);
           for j=1:no_of_particles
               %resampled_particle_indices(i,:)=randsample(no_of_particles,no_of_particles,true,unnormalized_weights(i,:));
               resampled_particle_indices(i,j)=acc_rej(unnormalized_weights(i,:)/(sum(unnormalized_weights(i,:))));
           end
       end
       
       for i=1:no_of_targets
           temp_vec=zeros(2*no_of_dimensions,1);
           for p=1:length(resampled_particle_indices(i,:))
               temp_vec=temp_vec+current_zeta_matrix(:,resampled_particle_indices(i,p),i);
               temp_current_zeta_matrix(:,p,i)=current_zeta_matrix(:,resampled_particle_indices(i,p),i);
           end
       end
       current_zeta_matrix=temp_current_zeta_matrix;