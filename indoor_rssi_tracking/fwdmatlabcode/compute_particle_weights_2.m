function[particle_weight_vec_of_a_target]=compute_particle_weights_1(target_observation_vec,eta_matrix,target_power_est,gamma_ln_est_matrix,target_index,no_of_targets,mote_locn,sigma_w)
[no_of_rows_target_obs_vec,no_of_motes]=size(target_observation_vec);
[no_of_rows_eta_matrix,no_of_particles]=size(eta_matrix);

delta = (45/(2^8));
sigma_w_quant = sigma_w + ((delta^2)/12);

for p=1:no_of_particles
    term=0;
    eta_matrix_of_given_target=extract_posn_vect(eta_matrix(:,p),3,1);
    eta_matrix_of_given_target(3,:)=0;
    target_posn=eta_matrix_of_given_target(:,target_index);
    cell_locn_of_target=target_locn(target_posn);
for n=1:no_of_motes
    dist_bw_mote_and_target=sum((target_posn - mote_locn(:,n)).^2)^(0.5);
    
    obs_given_curr_est=target_observation_vec(n)- target_power_est - gamma_ln_est_matrix(cell_locn_of_target,n) + 2*10*log10(dist_bw_mote_and_target);
    term=term + (obs_given_curr_est)^2;
end
%particle_weight_vec_of_a_target(p)=((2*pi*sigma_w^2)^(-no_of_motes/2))*exp((-1/(2*sigma_w^2))*term);
particle_weight_vec_of_a_target(p)=exp((-1/(2*sigma_w_quant^2))*term);
%((2*pi*sigma_w^2)^(-no_of_motes/2))*
end