function[eta_vectors_for_each_particle_each_user]=compute_eta_matrix(state_estimate_matrix,zeta_matrix)

[length_of_state_vec,no_of_particles,no_of_targets]=size(zeta_matrix);
%[no_of_targets,length_of_state_vec]=size(state_estimate_matrix);


for i=1:no_of_targets
    eta_matrix=state_estimate_matrix;
    for j=1:no_of_particles
        eta_matrix(i,:)=zeta_matrix(:,j,i);
        size(eta_matrix);
        eta_vectors_for_each_particle_each_user(:,j,i)=convert_eta_matrix_to_vec(eta_matrix);
    end
end