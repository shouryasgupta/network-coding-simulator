function[z_next]=test_rssi_tracking(no_of_targets,no_of_dimensions,T,proc_noise_var_db,previous_state_vec,epsilon_depend)


%creating the block diagonal A matrix;
c=[1 T;0 1];
%A_mat=cell(1,no_of_targets*no_of_dimensions);
%[A_mat{:}]=deal(sparse(c));
%A=full(blkdiag(A_mat{:}));
A=kron(eye(no_of_targets*no_of_dimensions),c);
B=kron(eye(no_of_targets*no_of_dimensions),[T^2/2, T/2]');
c_interaction_matrix=kron(kron((ones(no_of_targets,no_of_targets) - no_of_targets*eye(no_of_targets)),eye(no_of_dimensions)),[1 0]);
u=sqrt(10^(proc_noise_var_db/10))*randn(no_of_targets*no_of_dimensions,1);
%epsilon_depend*tanh(sinh(B*c_interaction_matrix*previous_state_vec)./100);
% size(A)
% size(previous_state_vec)
% size(B)
% size(u)
z_next=A*previous_state_vec + B*u + epsilon_depend*tanh(sinh(B*c_interaction_matrix*previous_state_vec)./100);

