function[current_owner_mote]=check_target_owner(previous_owner_mote,previous_state_estimate,...
                                                                                                                                      prev_neighbourhood_matrix,mote_location,aggr_radius)
 [lengthof_state_vec, no_of_targets]=size(previous_state_estimate);
 no_of_dimensions = lengthof_state_vec/2;
for m = 1:no_of_targets
    target_owner = previous_owner_mote(m);
    pos_vec=extract_posn_vect(previous_state_estimate(:,m),no_of_dimensions,1);
    pos_vec(3)=0;
    mote_location(3,target_owner)=0;
d = norm((mote_location(:,target_owner)-pos_vec),2);
if (d > aggr_radius)
    temp=previous_owner_mote(m);
    previous_owner_mote(m)= find_owner_of_given_target(previous_state_estimate(:,m),mote_location);
     %fprintf('Owner of the target %d changed from %d to %d\n', m,temp, previous_owner_mote(m));
    
%     prev_neighbourhood_matrix(:,m)=compute_target_neighbourhood(aggr_radius,previous_owner_mote(m),mote_location);
end
end
current_owner_mote = previous_owner_mote;



