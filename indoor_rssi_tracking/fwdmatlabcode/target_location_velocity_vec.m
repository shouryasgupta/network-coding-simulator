function[target_state_vec]=target_location_velocity_vec(cell_index,target_x_velocity,target_y_velocity,no_of_dimensions)
%cell_x_y_coorinates(:,:,1)=[0 0; 0+9 0+9];
%cell_x_y_coorinates(:,:,2)=[10 0;10+9 0+9];
%cell_x_y_coorinates(:,:,3)=[10 10;10+9 10+9];
%cell_x_y_coorinates(:,:,4)=[0 10;0+9 10+9];

cell_x_y_coorinates(:,:,1)=[0 0; 0+9 0+9];
cell_x_y_coorinates(:,:,2)=[10 0;10+9 0+9];
cell_x_y_coorinates(:,:,3)=[10 10;10+9 10+9];
cell_x_y_coorinates(:,:,4)=[0 10;0+9 10+9];

size(cell_x_y_coorinates);

for i=1:no_of_dimensions-1
    if i==1
    target_state_vec_first_locn=[cell_x_y_coorinates(1,i,cell_index)+(cell_x_y_coorinates(2,i,cell_index) - cell_x_y_coorinates(1,i,cell_index))*rand;target_x_velocity];
    else
        target_state_vec=[target_state_vec_first_locn;[cell_x_y_coorinates(1,i,cell_index)+(cell_x_y_coorinates(2,i,cell_index) - cell_x_y_coorinates(1,i,cell_index))*rand;target_y_velocity]];
    end
end

target_state_vec((2*no_of_dimensions)-1:2*no_of_dimensions)=0;