function[suff_observation_matrix]= sufficient_obs_matrix(observation_matrix,target_neighbourhood_matrix)

[no_of_targets,no_of_motes]=size(observation_matrix);
suff_observation_matrix = zeros(no_of_targets,no_of_motes);

for m = 1:no_of_targets
    obs_for_given_target = observation_matrix(m,:);
    neighbourhood_for_given_target = target_neighbourhood_matrix(m,:);
    l=1;
%     suff_observation_vec = [];
%     for i = 1: no_of_motes
        while (neighbourhood_for_given_target(l)~=0)
            suff_observation_matrix(m,l) = obs_for_given_target(neighbourhood_for_given_target(l));
            l=l+1;
        end
%     end
%     if size(suff_observation_vec) < max(size(neighbourhood_for_given_target))
%         suff_observation_vec = padarray(suff_observation_vec, 13 - max(size(suff_observation_vec)),'post');
%     end
%     suff_observation_matrix =[suff_observation_matrix , suff_observation_vec];
end

            
            