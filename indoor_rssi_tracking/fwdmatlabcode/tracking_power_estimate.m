function[final_output]=tracking_power_estimate(sigma_w_db,no_of_motes)
%clear all; 
%clc;
%close all;
a=0;
b=20;
no_of_targets=8;
target_locn_index=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];
no_of_dimensions=3;
no_of_particles=128;
no_of_cells=4;
%no_of_motes=9;
T=0.001;
sigma_sq_threhold_db=-10;
sigma_sq_u_db=-35;
%sigma_w_db=2;
epsilon_depend=0;
%vln=[0 0 1 1 1 2 1 2 2; 1 1 0 2 2 1 2 1 1; 2 2 1 1 1 0 1 0 0 ; 1 1 2 0 0 1 0 1 1];
vln=[0 0 1 1 1 2 1 2 2 0 0 1 1 1 2 1; 1 1 0 2 2 1 2 1 1 1 1 0 0 0 1 2; 2 2 1 1 1 0 1 0 0 2 2 1 1 1 0 1; 1 1 2 0 0 1 0 1 1 1 1 2 2 2 1 0];


%etaln=10^(3/20)*randn(no_of_cells,no_of_motes);
gamma_wall=-2;
phi_nominal=20;
  r(:,1) = [1; 0; 10];
  r(:,2) = [9; 0; 10];
  r(:,3) = [19; 0; 10];
  r(:,4) = [1; 11; 10];
  r(:,5) = [9; 11; 10];
  r(:,6) = [19; 11; 10];
  r(:,7) = [1; 19; 10];
  r(:,8) = [11; 19; 10];
  r(:,9) = [19; 19; 10];
  r(:,10)= [1; 9; 10];
  r(:,11)= [9; 9; 10];
  r(:,12)= [11; 0; 10];
  r(:,13)= [11; 9; 10];
  r(:,14)= [19; 9; 10];
  r(:,15)= [11; 11; 10];
  r(:,16)= [9; 19; 10];

%creation of static radio environment
%static_phi_m_vec=10.^(phi_nominal*ones(no_of_targets,1)./10) + (10^(3/20)*randn(no_of_targets,1)).^2;
static_phi_m_vec=phi_nominal*ones(no_of_targets,1) + 3*randn(no_of_targets,1); 
for l=1:no_of_cells
    for n=1:no_of_motes
        if (l ~= target_locn(r(:,n)))
            gamma_ln(l,n)=vln(l,n)*gamma_wall + 3*randn;
        else
            %gamma_ln(l,n)=vln(l,n)*(10^(gamma_wall/10));
            gamma_ln(l,n)=0;
        end
    end
end

true_parameter_est=[static_phi_m_vec];
% for m=1:1:no_of_targets
% for i=1:no_of_dimensions
%     z_col_temp=[rand, 1];
%     if i==1
%         z_col=z_col_temp';
%     else
%         z_col=[z_col;z_col_temp'];
%     end
% end
%     if m==1
%         initial_state_vec=z_col;
%     else
%         initial_state_vec=[initial_state_vec;z_col];
%    end
% end

for m=1:no_of_targets
    if m==1
        initial_state_vec=initial_target_location_velocity_vec(target_locn_index(m),1,1,no_of_dimensions);
    else
        initial_state_vec=[initial_state_vec;initial_target_location_velocity_vec(target_locn_index(m),1,1,no_of_dimensions)];
    
    end
end


%sampling_epoch=1;
previous_parameter_est=phi_nominal*ones(no_of_targets,1);
%parameter_est_error(sampling_epoch)=norm((true_parameter_est - previous_parameter_est),2)^2;
no_of_points=1500*2;
%figure;hold on;
%axis([0 20 0 20]);
%while(parameter_est_error(sampling_epoch) > 0.1)
   for sampling_epoch=1:1:no_of_points
       
       if(sampling_epoch==1)
state_vec=initial_state_vec;
% for ii=1:no_of_targets
%     %for jj=1:no_of_particles
%     previous_zeta_matrix(:,:,ii)=a+(b-a)*rand(2*no_of_dimensions,no_of_particles);
%     %previous_zeta_matrix(:,:,ii)=rand(2*no_of_dimensions,no_of_particles)
%     %end
% previous_state_estimate(ii,:)=compute_state_estimate(previous_zeta_matrix(:,:,ii));
% end
    
for ii=1:no_of_targets
    for jj=1:no_of_particles
        previous_zeta_matrix(:,jj,ii)=target_location_velocity_vec(target_locn_index(ii),1,1,3);
    end
       previous_state_estimate(ii,:)=compute_state_estimate(previous_zeta_matrix(:,:,ii)); 
end
       else  
next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_u_db,state_vec,epsilon_depend);
state_vec = next_state_vec;
       end
       %compute eta for the previous time instant
       previous_eta_matrix=compute_eta_matrix(previous_state_estimate,previous_zeta_matrix);
       %propagate eta for the current instant
       for i=1:no_of_targets
           for j=1:no_of_particles
       current_eta_matrix(:,j,i)=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_threhold_db,previous_eta_matrix(:,j,i),epsilon_depend);
           end
       end
       
       observation_matrix=obs_model(state_vec,no_of_dimensions,no_of_targets,no_of_motes,static_phi_m_vec,gamma_ln,r,sigma_w_db);
       %in the observation matrix above column index is the mote index,
       %while row index is the target index
       
       [no_of_targets,no_of_motes]=size(observation_matrix);
       %estimate the radio environment
       if sampling_epoch==1                      
          previous_parameter_est=phi_nominal*ones(no_of_targets,1);
       end
       previous_gamma_est_matrix=gamma_ln;
       %current_parameter_est=true_parameter_est;
       %previous_parameter_est(no_of_targets+1)=[];
       [score_fn_grad]=computation_of_gradient_of_scorefn(observation_matrix,previous_zeta_matrix,previous_parameter_est,previous_gamma_est_matrix,no_of_particles,r,sigma_w_db);
       current_parameter_est=previous_parameter_est + (0.001)*eye(no_of_targets)*score_fn_grad(1:no_of_targets);
       %current_parameter_est=previous_parameter_est + inv(compute_approximate_fisher_info_matrix(no_of_targets,no_of_cells,no_of_motes,sigma_w_db))*score_fn_grad;
       %assign the updated parameter values to the previous_power_est and
       %previous_gamma_est_matrix
       %previous_gamma_est_matrix(1,1)=1;
       previous_parameter_est=current_parameter_est; 
       
       %compute particle weights
       for i=1:no_of_targets
           for j=1:no_of_particles
               unnormalized_weights(i,:)=compute_particle_weights(observation_matrix(i,:),current_eta_matrix(:,:,i),previous_parameter_est(i),previous_gamma_est_matrix,i,no_of_targets,r,sigma_w_db);
               resampled_particle_indices(i,:)=randsample(no_of_particles,no_of_particles,true,unnormalized_weights(i,:));
           end
       end
       
       for m=1:no_of_targets
           for p=1:no_of_particles
               chi_matrix(:,p,m)=current_eta_matrix(:,resampled_particle_indices(m,p),m);
               project_chi_to_zeta_temp=vec2mat(chi_matrix(:,p,m),2*no_of_dimensions);
               current_zeta_matrix(:,p,m)=project_chi_to_zeta_temp(m,:)';
           end
           current_state_estimate(m,:)=compute_state_estimate(current_zeta_matrix(:,:,m));
       end
       previous_zeta_matrix=current_zeta_matrix;
       previous_state_estimate=current_state_estimate;
       

xn=extract_posn_vect(state_vec,no_of_dimensions,no_of_targets);
xn(3,:)=0;
target_posn_est=extract_posn_vect(reshape(current_state_estimate',1,2*no_of_dimensions*no_of_targets)',no_of_dimensions,no_of_targets);
target_posn_est(3,:)=0;

 for j=1:no_of_targets
     error_sq(j,sampling_epoch)=norm(xn(:,j)-target_posn_est(:,j),2)^2;
 end
tracking_meansq_error(sampling_epoch)=norm(reshape(xn,1,no_of_targets*no_of_dimensions)' - reshape(target_posn_est,1,no_of_targets*no_of_dimensions)',2)^2;
%for j=1:no_of_targets
%plot(xn(1,1),xn(2,1),'o','MarkerSize',5);
%plot(target_posn_est(1,1),target_posn_est(2,1),'ro','MarkerSize',5);
%plot(error_sq(j),'ro','MarkerSize',5);
%drawnow;
%end
%previous_state_estimate=next_state_estimate;
%previous_zeta_matrix=next_zeta_matrix;
%sampling_epoch=sampling_epoch +1;
parameter_est_error(sampling_epoch)=norm((true_parameter_est - current_parameter_est),2)^2;
parameter_est_error_current_epoch=parameter_est_error(sampling_epoch);
   end  
rms_tracking_error=sqrt((1/no_of_targets)*mean(error_sq(:,no_of_points)));
   %rms_tracking_error=sqrt(mean(tracking_meansq_error));
   final_output=[rms_tracking_error,parameter_est_error];
   %hold off;
%figure(2);semilogy(parameter_est_error,'o','MarkerSize',5); grid on;
%figure(3);plot(tracking_meansq_error,'MarkerSize',5); grid on;


%for 2-dimensional tracking set the values of initial_state_vec beyond 5 to
%"0".
%initial_state_vec(5:2*no_of_targets*no_of_dimensions)=zeros((((2*no_of_targets*no_of_dimensions)-5) + 1),1)



