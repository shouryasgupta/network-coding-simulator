function[current_owner_mote] = find_owner_of_given_target(previous_state_estimate,mote_location)

[length_of_state_vector, no_of_targets]=size(previous_state_estimate);
[no_of_dimensions,no_of_motes]=size(mote_location);
current_owner_mote =[];
% for m = 1:no_of_targets
%     post_vec(:,m) = extract_position_vector(state_vector,no_of_dimensions,no_of_targets)
% owner_for_target = current_owner_mote(m);
post_vec=extract_posn_vect(previous_state_estimate',no_of_dimensions,1) ;
    post_vec(3) = 0;
   for n = 1:no_of_motes
       mote_location(3,n)=0;
        distance_vec_from_motes_to_target(n) = norm((post_vec - mote_location(:,n)),2);
        
   end
   [distance_from_target_to_nearest_mote, nearest_mote_index]=min(distance_vec_from_motes_to_target);
    current_owner_mote = nearest_mote_index;
    
% end
