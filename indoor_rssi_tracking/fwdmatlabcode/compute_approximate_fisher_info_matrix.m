function[Hessian_matrix]=compute_approximate_fisher_info_matrix(M,L,N,sigma_w_db,no_of_parameters_not_estimated)
%M - no. of targets
%L - no. of cells
%N - no. of motes
% sigma_w=10^(sigma_w_db/10)
% F_theta=eye(M + ((L*N)-1));
% F_theta([1:M],[1:M])=N*eye(M);
% F_theta([M+1:M + ((L*N)-1)],[M+1:M+((L*N)-1)])=(M/L)*eye((L*N)-1);
% F_theta([1:M],[M+1:M+((L*N)-1)])=(1/L)*ones(M,((L*N)-1));
% F_theta([(M+1):M+((L*N)-1)],[1:M])=(1/L)*ones(((L*N)-1),M);
% F_theta=(1/sigma_w)^2*F_theta;

block_matrix_1=(N/(sigma_w_db^2))*eye(M);
block_matrix_2=(1/L)*(1/(sigma_w_db^2))*ones(M,((L*N)-no_of_parameters_not_estimated));
block_matrix_3=block_matrix_2';
block_matrix_4=(M/L)*(1/(sigma_w_db^2))*eye(((L*N)-no_of_parameters_not_estimated));
Hessian_matrix_1=horzcat(block_matrix_1,block_matrix_2);
Hessian_matrix_2=horzcat(block_matrix_3,block_matrix_4);
Hessian_matrix=vertcat(Hessian_matrix_1,Hessian_matrix_2);


