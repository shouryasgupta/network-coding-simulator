function[target_state_vec]=initial_target_location_velocity_vec(cell_index,target_x_velocity,target_y_velocity,no_of_dimensions)
cell_x_y_coorinates(:,:,1)=[-50 -50; -50+2 -50+2];
cell_x_y_coorinates(:,:,2)=[0 0;0+2 0+2];
cell_x_y_coorinates(:,:,3)=[11 11;11+2 11+2];
cell_x_y_coorinates(:,:,4)=[0 11;0+2 11+2];



% cell_x_y_coorinates(:,:,1)=[0 0; 0+20 0+20];
% cell_x_y_coorinates(:,:,2)=[0 0;0+20 0+20];
% cell_x_y_coorinates(:,:,3)=[0 0;0+20 0+20];
% cell_x_y_coorinates(:,:,4)=[0 0;0+20 0+20];

size(cell_x_y_coorinates);

for i=1:no_of_dimensions-1
    if i==1
    target_state_vec_first_locn=[cell_x_y_coorinates(1,i,cell_index)+(cell_x_y_coorinates(2,i,cell_index) - cell_x_y_coorinates(1,i,cell_index))*rand;target_x_velocity];
    else
        target_state_vec=[target_state_vec_first_locn;[cell_x_y_coorinates(1,i,cell_index)+(cell_x_y_coorinates(2,i,cell_index) - cell_x_y_coorinates(1,i,cell_index))*rand;target_y_velocity]];
    end
end

target_state_vec((2*no_of_dimensions)-1:2*no_of_dimensions)=0;