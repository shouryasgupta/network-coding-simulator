clear all; 
clc;
close all;
a=0;
b=20;
no_of_targets=8;
target_locn_index=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];
%target_locn_index=[1 2 1 2 1 1 1 1 1 1 1 1 1 1 1 1];
no_of_dimensions=3;
no_of_particles=128;
no_of_particles_1=128;
no_of_cells=4;
no_of_motes=16;
T=1;
sigma_sq_threhold_db=-10;
sigma_sq_u_db=-35;
sigma_w_db=5;
epsilon_depend=0;
vln=[0 0 1 1 1 2 1 2 2 0 0 1 1 1 2 1; 1 1 0 2 2 1 2 1 1 1 1 0 0 0 1 2; 2 2 1 1 1 0 1 0 0 2 2 1 1 1 0 1; 1 1 2 0 0 1 0 1 1 1 1 2 2 2 1 0];

%etaln=10^(3/20)*randn(no_of_cells,no_of_motes);
gamma_wall=-2;
phi_nominal=5*ones(no_of_targets,1);
  r(:,1) = [1; 0; 10];
  r(:,2) = [9; 0; 10];
  r(:,3) = [19; 0; 10];
  r(:,4) = [1; 11; 10];
  r(:,5) = [9; 11; 10];
  r(:,6) = [19; 11; 10];
  r(:,7) = [1; 19; 10];
  r(:,8) = [11; 19; 10];
  r(:,9) = [19; 19; 10];
  r(:,10)= [1; 9; 10];
  r(:,11)= [9; 9; 10];
  r(:,12)= [11; 0; 10];
  r(:,13)= [11; 9; 10];
  r(:,14)= [19; 9; 10];
  r(:,15)= [11; 11; 10];
  r(:,16)= [9; 19; 10];


%creation of static radio environment
%static_phi_m_vec=10.^(phi_nominal*ones(no_of_targets,1)./10) + (10^(3/20)*randn(no_of_targets,1)).^2;
static_phi_m_vec=phi_nominal.*ones(no_of_targets,1) + 3*randn(no_of_targets,1); 
for l=1:no_of_cells
    for n=1:no_of_motes
        if (l ~= target_locn(r(:,n)))
            gamma_ln(l,n)=vln(l,n)*gamma_wall + 3*randn;
            gamma_ln_initial_for_est(l,n)=vln(l,n)*gamma_wall;
        else
            %gamma_ln(l,n)=vln(l,n)*(10^(gamma_wall/10));
            gamma_ln(l,n)=0;
            gamma_ln_initial_for_est(l,n)=0;
        end
    end
end

gamma_ln_initial_for_est_vec=reshape(gamma_ln_initial_for_est',no_of_cells*no_of_motes,1);
gamma_ln_vec=reshape(gamma_ln',no_of_cells*no_of_motes,1);
gamma_ln_initial_for_est_vec_trimmed=[];
gamma_ln_vec_trimmed=[];
no_of_parameters_not_estimated=0;
for l=1:length(gamma_ln_initial_for_est_vec)
    if gamma_ln_initial_for_est_vec(l)~=0
        gamma_ln_initial_for_est_vec_trimmed=[gamma_ln_initial_for_est_vec_trimmed;gamma_ln_initial_for_est_vec(l)];
        gamma_ln_vec_trimmed=[gamma_ln_vec_trimmed;gamma_ln_vec(l)];
    else
        no_of_parameters_not_estimated=no_of_parameters_not_estimated + 1;
    end
end

gamma_ln_initial_for_est_vec_trimmed_1=gamma_ln_initial_for_est_vec_trimmed;
true_parameter_est=[static_phi_m_vec;gamma_ln_vec_trimmed];


for p=1:no_of_particles_1
    initial_state_vec=[];
for m=1:no_of_targets
    if m==1
        %x_vel_initial=-0.5+rand;y_vel_initial=-0.5+rand;
        x_vel_initial=0.5;y_vel_initial=0.5;
        initial_state_vec=initial_target_location_velocity_vec(target_locn_index(m),x_vel_initial,y_vel_initial,no_of_dimensions);
    else
        %x_vel_initial=-0.5+rand;y_vel_initial=-0.5+rand;
        x_vel_initial=0.5;y_vel_initial=0.5;
        initial_state_vec=[initial_state_vec;initial_target_location_velocity_vec(target_locn_index(m),x_vel_initial,y_vel_initial,no_of_dimensions)];
    
    end
end
past_true_state_particle_matrix(:,:,p)=reshape(initial_state_vec,2*no_of_dimensions,no_of_targets);
end


approximate_fisher_info_matrix=compute_approximate_fisher_info_matrix(no_of_targets,no_of_cells,no_of_motes,sigma_w_db,no_of_parameters_not_estimated);

no_of_points=1500;
%figure;hold on;
%axis([0 20 0 20]);
plot_index=1;
   for sampling_epoch=1:1:no_of_points
       sampling_epoch;
       if(sampling_epoch==1)
           previous_observation_matrix=obs_model(initial_state_vec,no_of_dimensions,no_of_targets,no_of_motes,static_phi_m_vec,gamma_ln,r,sigma_w_db);
state_vec=initial_state_vec;
previous_filter_pdf_weights=(1/no_of_particles)*ones(no_of_targets,no_of_particles);

%for ii=1:no_of_targets
    %for jj=1:no_of_particles
    %previous_zeta_matrix(:,:,ii)=a+(b-a)*rand(2*no_of_dimensions,no_of_particles);
    %previous_zeta_matrix(:,:,ii)=1+6*rand(2*no_of_dimensions,no_of_particles)
    %end
%previous_state_estimate(ii,:)=compute_state_estimate(previous_zeta_matrix(:,:,ii));
%end

for i=1:no_of_targets           
           for j=1:no_of_particles
               %resampled_particle_indices(i,:)=randsample(no_of_particles,no_of_particles,true,unnormalized_weights(i,:));
               resampled_particle_indices(i,j)=acc_rej(previous_filter_pdf_weights(i,:)/(sum(previous_filter_pdf_weights(i,:))));
           end
end
       resampled_particle_indices_1=resampled_particle_indices;
    
for ii=1:no_of_targets
    for jj=1:no_of_particles
        %x_vel=-0.5+rand;y_vel=-0.5+rand;
        x_vel=0.5;y_vel=0.5;
        previous_zeta_matrix(:,jj,ii)=target_location_velocity_vec(target_locn_index(ii),x_vel,y_vel,3);
    end
       
end
previous_zeta_matrix_1=previous_zeta_matrix;
       else
           
next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_u_db,state_vec,epsilon_depend);
next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,state_vec,T,sigma_sq_u_db,epsilon_depend);
state_vec = next_state_vec;
       end
       target_state_matrix=reshape(state_vec,2*no_of_dimensions,no_of_targets);
       observation_matrix=obs_model(state_vec,no_of_dimensions,no_of_targets,no_of_motes,static_phi_m_vec,gamma_ln,r,sigma_w_db);
       
       %estimation of the radio environment
       if sampling_epoch==1      
           previous_gamma_est_matrix=gamma_ln_initial_for_est;     
           previous_gamma_est_matrix_1=gamma_ln_initial_for_est;     
           previous_power_est=phi_nominal.*ones(no_of_targets,1);
           previous_power_est_1=phi_nominal.*ones(no_of_targets,1);
       end
       step_size=0.01;
       [current_parameter_est,previous_power_est,previous_gamma_est_matrix,current_zeta_matrix,resampled_particle_indices]=radio_env_estimation(previous_power_est,previous_gamma_est_matrix,previous_observation_matrix,observation_matrix,previous_zeta_matrix,resampled_particle_indices,gamma_ln_initial_for_est_vec_trimmed,approximate_fisher_info_matrix,T,sigma_sq_threhold_db,r,sigma_w_db,vln,step_size,'regular');
       [current_parameter_est_1,previous_power_est_1,previous_gamma_est_matrix_1,current_zeta_matrix_1,resampled_particle_indices_1]=radio_env_estimation(previous_power_est_1,previous_gamma_est_matrix_1,previous_observation_matrix,observation_matrix,previous_zeta_matrix_1,resampled_particle_indices_1,gamma_ln_initial_for_est_vec_trimmed_1,approximate_fisher_info_matrix,T,sigma_sq_threhold_db,r,sigma_w_db,vln,step_size,'new');
       previous_gamma_est_matrix_vec=current_parameter_est(no_of_targets+1:length(current_parameter_est));
       previous_gamma_est_matrix_vec_1=current_parameter_est_1(no_of_targets+1:length(current_parameter_est_1));
       gamma_ln_initial_for_est_vec_trimmed=previous_gamma_est_matrix_vec;
       gamma_ln_initial_for_est_vec_trimmed_1=previous_gamma_est_matrix_vec_1;
       
       parameter_est_error(sampling_epoch)=norm((true_parameter_est - current_parameter_est),1);
       parameter_est_error_1(sampling_epoch)=norm((true_parameter_est - current_parameter_est_1),1);
       
       if (mod(sampling_epoch,100)==0)
       fprintf('Sampling epoch: %d, estimation error (regular gradient): %f and estimation error (new method): %f\n',sampling_epoch,parameter_est_error(sampling_epoch),parameter_est_error_1(sampling_epoch));
       end
       
       if (sampling_epoch==1 || mod(sampling_epoch,75)==0)
           plot_parameter_est_error_vec_natural(plot_index)=parameter_est_error(sampling_epoch);
           plot_parameter_est_error_vec_regular(plot_index)=parameter_est_error_1(sampling_epoch);
           
           if (sampling_epoch==1)
               plot_xaxis(plot_index)=0;
           else
               plot_xaxis(plot_index)=sampling_epoch;
           end
           plot_index=plot_index + 1;
       end
         
       previous_zeta_matrix=current_zeta_matrix;
       previous_zeta_matrix_1=current_zeta_matrix_1;
       previous_observation_matrix=observation_matrix;
   end
   
   figure(1);
   semilogy(plot_xaxis,plot_parameter_est_error_vec_natural,'k-o','MarkerSize',10,'LineWidth',2);grid on; hold on;
   xlabel('Number of iterations t','fontsize',18,'fontweight','b');
   ylabel('Absolute error of the total estimated parameter','fontsize',18,'fontweight','b');
   semilogy(plot_xaxis,plot_parameter_est_error_vec_regular,'r--s','MarkerSize',10,'LineWidth',2);
   hleg1=legend('Regular','New');
   set(hleg1,'FontSize',18);
   hold off;
  

