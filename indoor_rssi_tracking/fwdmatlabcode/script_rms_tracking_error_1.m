clear all;
clc;
sigma_w_dB_vec=[2 3 4 5 6 7 8];
no_of_motes=[3 4 5 6 7 8 9 10 11 12 13 14 15 16];

for i=1:length(sigma_w_dB_vec)
        [output_tracking_algorithm]=tracking_power_estimate(sigma_w_dB_vec(i),16);
    rms_tracking_error_vec(i)=output_tracking_algorithm(1);
    %no_of_iterations_vec(i)=output_tracking_algorithm(2)
    %parameter_est_error_one_norm_value_vec(i)=output_tracking_algorithm(3);
    sprintf('Done: Sigma= %d, RMS tracking error= %f',sigma_w_dB_vec(i),rms_tracking_error_vec(i))
    end

% for j=1:length(no_of_motes)
%         [output_tracking_algorithm_1]=tracking_power_estimate(2,no_of_motes(j));
%     rms_tracking_error_vec_1(j)=output_tracking_algorithm_1(1)
%     %no_of_iterations_vec(i)=output_tracking_algorithm(2)
%     %parameter_est_error_one_norm_value_vec(i)=output_tracking_algorithm(3);
%     sprintf('Done: Number of Motes= %d, RMS tracking error= %f',no_of_motes(j),rms_tracking_error_vec_1(j));
%     end

%
%rms_tracking_error = 0.359008 (for sigma_w=1 and no_of_motes=9)
figure(1)
plot(sigma_w_dB_vec,rms_tracking_error_vec,'o-','MarkerSize',5);
grid on;
xlabel('Noise standard deviation \sigma_w in dB');
ylabel('RMS tracking error in meters');
% figure(2)
% plot(no_of_motes,rms_tracking_error_vec_1,'o-','MarkerSize',5);
% grid on;
% xlabel('Number of Motes');
% ylabel('RMS tracking error in meters');