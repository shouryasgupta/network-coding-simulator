function[samples_from_predictor_pdf]=generate_samples_from_predictor_pdf(previous_filter_pdf_weights,previous_zeta_matrix,no_of_targets,no_of_particles,no_of_dimensions,T,proc_noise_var_db,epsilon_depend)

for i=1:no_of_targets
    for j=1:no_of_particles
        particle_index=acc_rej(previous_filter_pdf_weights(i,:)/(sum(previous_filter_pdf_weights(i,:))));
        past_state_matrix(:,j,i)=previous_zeta_matrix(:,particle_index,i);
    end
           
end
       
       for j=1:no_of_particles
           for i=1:no_of_targets
               if (i == 1)
                   total_state_vec=past_state_matrix(:,j,i);
               else
                   total_state_vec=[total_state_vec;past_state_matrix(:,j,i)];
               end
           end
           next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,proc_noise_var_db,total_state_vec,epsilon_depend);
samples_from_predictor_pdf(:,j,:)=reshape(next_state_vec,2*no_of_dimensions,no_of_targets);
       end