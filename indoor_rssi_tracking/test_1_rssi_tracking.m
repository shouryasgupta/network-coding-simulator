clear all;
clc;
no_of_targets=2;
no_of_dimensions=3;
T=1;

for m=1:1:no_of_targets
for i=1:no_of_dimensions
    z_col_temp=[20*rand, 10];
    if i==1
        z_col=z_col_temp';
    else
        z_col=[z_col;z_col_temp'];
    end
end
    if m==1
        initial_state_vec=z_col;
    else
        initial_state_vec=[initial_state_vec;z_col];
   end
end

%for 2-dimensional tracking set the values of initial_state_vec beyond 5 to
%"0".
%initial_state_vec(5:2*no_of_targets*no_of_dimensions)=zeros((((2*no_of_targets*no_of_dimensions)-5) + 1),1)

next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,initial_state_vec);

