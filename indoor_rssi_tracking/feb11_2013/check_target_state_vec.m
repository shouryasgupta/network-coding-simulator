function[current_state_vec]=check_target_state_vec(current_state_vec,no_of_dimensions,no_of_targets,previous_state_vec,T,sigma_sq_u_db,epsilon_depend)
current_state_vec_matrix=reshape(current_state_vec,2*no_of_dimensions,no_of_targets);
previous_state_vec_matrix=reshape(previous_state_vec,2*no_of_dimensions,no_of_targets);


for j=1:no_of_targets
    posn_vect=extract_posn_vect(current_state_vec_matrix(:,j),no_of_dimensions,1);
    no_of_iterations=0;
    x_coord=posn_vect(1);y_coord=posn_vect(2);
    temp_current_state_vec_matrix=current_state_vec_matrix;
    while((x_coord > 20 | x_coord < 0) || (y_coord > 20 | y_coord < 0))
        %fprintf('Target %d out of tracking area. X-coordinate: %f Y- coordinate: %f\n', j,posn_vect(1), posn_vect(2));
        if x_coord > 20
            previous_state_vec_matrix(2,j)= -0.5;
        elseif x_coord < 0
            previous_state_vec_matrix(2,j)= 0.5;
        end
        if y_coord > 20
            previous_state_vec_matrix(4,j)= -0.5;
        elseif y_coord < 0
            previous_state_vec_matrix(4,j)= 0.5;
        end
        previous_state_vec=reshape(previous_state_vec_matrix,2*no_of_targets*no_of_dimensions,1);
        temp_current_state_vec_matrix=reshape(test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_u_db,previous_state_vec,epsilon_depend),2*no_of_dimensions,no_of_targets);
        posn_vect_1=extract_posn_vect(temp_current_state_vec_matrix(:,j),no_of_dimensions,1);
    x_coord=posn_vect_1(1);y_coord=posn_vect_1(2);
    no_of_iterations=no_of_iterations+1;
    end
    if (no_of_iterations ~=0)
    %fprintf('Target %d brought back into tracking area. X-coordinate: %f Y- coordinate: %f\n', j,x_coord,y_coord);
    end
    current_state_vec_matrix(:,j)=temp_current_state_vec_matrix(:,j);
end
current_state_vec=reshape(current_state_vec_matrix,2*no_of_targets*no_of_dimensions,1);





%for j=1:no_of_targets
%   posn_vect=extract_posn_vect(current_state_vec_matrix(:,j),no_of_dimensions,1);
%   posn_vect(3)=[];
%     if (posn_vect(1) > 20 | posn_vect(1) < 0)
%         fprintf('Target %d out of tracking area. X-coordinate: %f Y- coorinate: %f\n', j,posn_vect(1), posn_vect(2));
%         previous_state_vec_matrix(2,j)=-previous_state_vec_matrix(2,j);
%     elseif(posn_vect(2) > 20 | posn_vect(2) < 0)
%         fprintf('Target %d out of tracking area. X-coordinate: %f Y- coorinate: %f\n', j, posn_vect(1), posn_vect(2));
%         previous_state_vec_matrix(4,j)=-previous_state_vec_matrix(4,j);
% if (posn_vect(1) > 20)
%         previous_state_vec_matrix(2,j)= -rand;
%         fprintf('Target %d out of tracking area. X-coordinate: %f Y- coorinate: %f\n', j,posn_vect(1), posn_vect(2));
% end
% if (posn_vect(1) < 0)
%         previous_state_vec_matrix(2,j)= rand;
%         fprintf('Target %d out of tracking area. X-coordinate: %f Y- coorinate: %f\n', j,posn_vect(1), posn_vect(2));
% end
% if (posn_vect(2) > 20)
%         previous_state_vec_matrix(2,j)= -rand;
%         fprintf('Target %d out of tracking area. X-coordinate: %f Y- coorinate: %f\n', j,posn_vect(1), posn_vect(2));
% end
% if (posn_vect(2) < 0)
%         previous_state_vec_matrix(2,j)= rand;
%         fprintf('Target %d out of tracking area. X-coordinate: %f Y- coorinate: %f\n', j,posn_vect(1), posn_vect(2));  
% end
% end
% 
% previous_state_vec_matrix-temp_previous_state_vec_matrix
% 
% if ((previous_state_vec_matrix-temp_previous_state_vec_matrix)~=zeros(2*no_of_dimensions,no_of_targets))
%     previous_state_vec=reshape(previous_state_vec_matrix,2*no_of_targets*no_of_dimensions,1);
% current_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_u_db,previous_state_vec,epsilon_depend);
% current_state_vec_matrix_adjusted=reshape(current_state_vec,2*no_of_dimensions,no_of_targets);
% for l=1:no_of_targets
%     if ((current_state_vec_adjusted(:,l)-current_state_vec_matrix(:,l))~=0)
%     posn_vect=extract_posn_vect(current_state_vec_matrix_adjusted(:,l),no_of_dimensions,1);
%     posn_vect(3)=[];
%     fprintf('Target %d brought back into the tracking area. X-coordinate: %f Y- coorinate: %f\n', l, posn_vect(1), posn_vect(2));
%     end
% end
% else
%     current_state_vec=reshape(current_state_vec_matrix,2*no_of_dimensions*no_of_targets,1);
% end

