clear all; 
clc;
close all;
a=0;
b=20;
no_of_targets=8;
target_locn_index=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];
%target_locn_index=[1 2 1 2 1 1 1 1 1 1 1 1 1 1 1 1];
no_of_dimensions=3;
no_of_particles=128;
no_of_particles_1=512;
no_of_cells=4;
no_of_motes=16;
T=1;
sigma_sq_threhold_db=-10;
sigma_sq_u_db=-35;
sigma_w_db=1;
epsilon_depend=0;
vln=[0 0 1 1 1 2 1 2 2 0 0 1 1 1 2 1; 1 1 0 2 2 1 2 1 1 1 1 0 0 0 1 2; 2 2 1 1 1 0 1 0 0 2 2 1 1 1 0 1; 1 1 2 0 0 1 0 1 1 1 1 2 2 2 1 0];

%etaln=10^(3/20)*randn(no_of_cells,no_of_motes);
gamma_wall=-2;
phi_nominal=5*ones(no_of_targets,1);
  r(:,1) = [1; 0; 10];
  r(:,2) = [9; 0; 10];
  r(:,3) = [19; 0; 10];
  r(:,4) = [1; 11; 10];
  r(:,5) = [9; 11; 10];
  r(:,6) = [19; 11; 10];
  r(:,7) = [1; 19; 10];
  r(:,8) = [11; 19; 10];
  r(:,9) = [19; 19; 10];
  r(:,10)= [1; 9; 10];
  r(:,11)= [9; 9; 10];
  r(:,12)= [11; 0; 10];
  r(:,13)= [11; 9; 10];
  r(:,14)= [19; 9; 10];
  r(:,15)= [11; 11; 10];
  r(:,16)= [9; 19; 10];


%creation of static radio environment
%static_phi_m_vec=10.^(phi_nominal*ones(no_of_targets,1)./10) + (10^(3/20)*randn(no_of_targets,1)).^2;
static_phi_m_vec=phi_nominal.*ones(no_of_targets,1) + 3*randn(no_of_targets,1); 
for l=1:no_of_cells
    for n=1:no_of_motes
        if (l ~= target_locn(r(:,n)))
            gamma_ln(l,n)=vln(l,n)*gamma_wall + 3*randn;
            gamma_ln_initial_for_est(l,n)=vln(l,n)*gamma_wall;
        else
            %gamma_ln(l,n)=vln(l,n)*(10^(gamma_wall/10));
            gamma_ln(l,n)=0;
            gamma_ln_initial_for_est(l,n)=0;
        end
    end
end

gamma_ln_initial_for_est_vec=reshape(gamma_ln_initial_for_est',no_of_cells*no_of_motes,1);
gamma_ln_vec=reshape(gamma_ln',no_of_cells*no_of_motes,1);
gamma_ln_initial_for_est_vec_trimmed=[];
gamma_ln_vec_trimmed=[];
for l=1:length(gamma_ln_initial_for_est_vec)
    if gamma_ln_initial_for_est_vec(l)~=0
        gamma_ln_initial_for_est_vec_trimmed=[gamma_ln_initial_for_est_vec_trimmed;gamma_ln_initial_for_est_vec(l)];
        gamma_ln_vec_trimmed=[gamma_ln_vec_trimmed;gamma_ln_vec(l)];
    end
end

true_parameter_est=[static_phi_m_vec;gamma_ln_vec_trimmed];


for p=1:no_of_particles_1
    initial_state_vec=[];
for m=1:no_of_targets
    if m==1
        %x_vel_initial=-0.5+rand;y_vel_initial=-0.5+rand;
        x_vel_initial=0.5;y_vel_initial=0.5;
        initial_state_vec=initial_target_location_velocity_vec(target_locn_index(m),x_vel_initial,y_vel_initial,no_of_dimensions);
    else
        %x_vel_initial=-0.5+rand;y_vel_initial=-0.5+rand;
        x_vel_initial=0.5;y_vel_initial=0.5;
        initial_state_vec=[initial_state_vec;initial_target_location_velocity_vec(target_locn_index(m),x_vel_initial,y_vel_initial,no_of_dimensions)];
    
    end
end
past_true_state_particle_matrix(:,:,p)=reshape(initial_state_vec,2*no_of_dimensions,no_of_targets);
end



no_of_points=10000;
% figure;hold on;
% axis([0 20 0 20]);

   for sampling_epoch=1:1:no_of_points
       sampling_epoch
       if(sampling_epoch==1)
state_vec=initial_state_vec;
previous_filter_pdf_weights=(1/no_of_particles)*ones(no_of_targets,no_of_particles);

%for ii=1:no_of_targets
    %for jj=1:no_of_particles
    %previous_zeta_matrix(:,:,ii)=a+(b-a)*rand(2*no_of_dimensions,no_of_particles);
    %previous_zeta_matrix(:,:,ii)=1+6*rand(2*no_of_dimensions,no_of_particles)
    %end
%previous_state_estimate(ii,:)=compute_state_estimate(previous_zeta_matrix(:,:,ii));
%end
    
for ii=1:no_of_targets
    for jj=1:no_of_particles
        %x_vel=-0.5+rand;y_vel=-0.5+rand;
        x_vel=0.5;y_vel=0.5;
        previous_zeta_matrix(:,jj,ii)=target_location_velocity_vec(target_locn_index(ii),x_vel,y_vel,3);
    end
       
end
       else  
next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_u_db,state_vec,epsilon_depend);
next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,state_vec,T,sigma_sq_u_db,epsilon_depend);
state_vec = next_state_vec;
       end
       target_state_matrix=reshape(state_vec,2*no_of_dimensions,no_of_targets);
       observation_matrix=obs_model(state_vec,no_of_dimensions,no_of_targets,no_of_motes,static_phi_m_vec,gamma_ln,r,sigma_w_db);
      %generation of particles for the current epoch for each target using
      %the kinematic prior
      for pp=1:no_of_particles
          previous_zeta_vec=reshape(previous_zeta_matrix(:,pp,:),2*no_of_dimensions*no_of_targets,1);
          next_state_vec=test_rssi_tracking(no_of_targets,no_of_dimensions,T,sigma_sq_threhold_db,previous_zeta_vec,0);
          next_state_vec=check_target_state_vec(next_state_vec,no_of_dimensions,no_of_targets,previous_zeta_vec,T,sigma_sq_threhold_db,0);
          current_zeta_matrix(:,pp,:)=reshape(next_state_vec,2*no_of_dimensions,no_of_targets);         
      end
      %compute particle weights and do the resampling
       for i=1:no_of_targets
           unnormalized_weights(i,:)=compute_particle_weights_1(observation_matrix(i,:),current_zeta_matrix(:,:,i),static_phi_m_vec(i),gamma_ln,1,no_of_targets,r,sigma_w_db);
           for j=1:no_of_particles
               %resampled_particle_indices(i,:)=randsample(no_of_particles,no_of_particles,true,unnormalized_weights(i,:));
               resampled_particle_indices(i,j)=acc_rej(unnormalized_weights(i,:)/(sum(unnormalized_weights(i,:))));
           end
       end
       for i=1:no_of_targets
           temp_vec=zeros(2*no_of_dimensions,1);
           for p=1:length(resampled_particle_indices(i,:))
               temp_vec=temp_vec+current_zeta_matrix(:,resampled_particle_indices(i,p),i);
               temp_current_zeta_matrix(:,p,i)=current_zeta_matrix(:,resampled_particle_indices(i,p),i);
           end
           current_state_estimate(:,i)=(1/no_of_particles)*(temp_vec);
           est_posn_vec(:,i)=extract_posn_vect(current_state_estimate(:,i),3,1);
           true_posn_vec(:,i)=extract_posn_vect(target_state_matrix(:,i),3,1);           
           targetposn_errorsq_matrix(i,sampling_epoch)=norm(([true_posn_vec(1,i);true_posn_vec(2,i)]-[est_posn_vec(1,i);est_posn_vec(2,i)]),2)^2;
       end
       current_zeta_matrix=temp_current_zeta_matrix;
       previous_zeta_matrix=current_zeta_matrix;
       if (sampling_epoch > 9500)
%            plot(true_posn_vec(1,1),true_posn_vec(2,1),'o','MarkerSize',5);
%            plot(est_posn_vec(1,1),est_posn_vec(2,1),'ro','MarkerSize',5);
%            drawnow;
           true_posn_vec_each_instant(:,sampling_epoch)=[true_posn_vec(1,1);true_posn_vec(2,1)];
           est_posn_vec_each_instant(:,sampling_epoch)=[est_posn_vec(1,1);est_posn_vec(2,1)];
         end
     
%             plot(true_posn_vec(1,1),true_posn_vec(2,1),'o','MarkerSize',5);
%             plot(est_posn_vec(1,1),est_posn_vec(2,1),'ro','MarkerSize',5);
%             drawnow;

   end
   %computation of RMS tracking error
   
for i=1:no_of_targets
    target_mse(i)=(1/no_of_points)*sum(targetposn_errorsq_matrix(i,:));
end
rms_tracking_error=sqrt((1/no_of_targets)*sum(target_mse));
%figure(1);

