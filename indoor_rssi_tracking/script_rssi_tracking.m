clear all;
clc;
no_of_targets=10;
no_of_dimensions=3;
K_temp=1;
while (K_temp <=no_of_targets)
    for i=1:no_of_dimensions
        if i== 1 | i==3
            z_vector(K_temp*i)=-10 + 20*rand;
        else
            z_vector(K_temp*i)=10;
        end
    end
    K_temp=K_temp+1
end